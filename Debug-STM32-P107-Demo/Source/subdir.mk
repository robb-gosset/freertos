################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/croutine.c \
../Source/event_groups.c \
../Source/list.c \
../Source/queue.c \
../Source/tasks.c \
../Source/timers.c 

OBJS += \
./Source/croutine.o \
./Source/event_groups.o \
./Source/list.o \
./Source/queue.o \
./Source/tasks.o \
./Source/timers.o 

C_DEPS += \
./Source/croutine.d \
./Source/event_groups.d \
./Source/list.d \
./Source/queue.d \
./Source/tasks.d \
./Source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Source/%.o: ../Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


