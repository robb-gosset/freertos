################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/loader_init.asm \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/vector.asm 

C_SRCS += \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/exit.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/loader_init2.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_atcm_init.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_cpg.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_ecm.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_icu_init.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_ram_init.c \
../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_reset.c 

OBJS += \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/exit.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/loader_init.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/loader_init2.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_atcm_init.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_cpg.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_ecm.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_icu_init.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_ram_init.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_reset.o \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/vector.o 

ASM_DEPS += \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/loader_init.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/vector.d 

C_DEPS += \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/exit.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/loader_init2.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_atcm_init.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_cpg.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_ecm.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_icu_init.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_ram_init.d \
./Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/r_reset.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/%.o: ../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/%.o: ../Demo/CORTEX_R4F_RZ_T_GCC_IAR/System/IAR/src/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


