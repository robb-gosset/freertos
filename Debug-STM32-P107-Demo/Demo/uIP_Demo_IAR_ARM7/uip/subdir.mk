################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/uIP_Demo_IAR_ARM7/uip/crt0.asm 

C_SRCS += \
../Demo/uIP_Demo_IAR_ARM7/uip/cgi.c \
../Demo/uIP_Demo_IAR_ARM7/uip/fs.c \
../Demo/uIP_Demo_IAR_ARM7/uip/fsdata.c \
../Demo/uIP_Demo_IAR_ARM7/uip/httpd.c \
../Demo/uIP_Demo_IAR_ARM7/uip/memb.c \
../Demo/uIP_Demo_IAR_ARM7/uip/slipdev.c \
../Demo/uIP_Demo_IAR_ARM7/uip/tapdev.c \
../Demo/uIP_Demo_IAR_ARM7/uip/telnetd-shell.c \
../Demo/uIP_Demo_IAR_ARM7/uip/telnetd.c \
../Demo/uIP_Demo_IAR_ARM7/uip/uip.c \
../Demo/uIP_Demo_IAR_ARM7/uip/uip_arch.c \
../Demo/uIP_Demo_IAR_ARM7/uip/uip_arp.c 

OBJS += \
./Demo/uIP_Demo_IAR_ARM7/uip/cgi.o \
./Demo/uIP_Demo_IAR_ARM7/uip/crt0.o \
./Demo/uIP_Demo_IAR_ARM7/uip/fs.o \
./Demo/uIP_Demo_IAR_ARM7/uip/fsdata.o \
./Demo/uIP_Demo_IAR_ARM7/uip/httpd.o \
./Demo/uIP_Demo_IAR_ARM7/uip/memb.o \
./Demo/uIP_Demo_IAR_ARM7/uip/slipdev.o \
./Demo/uIP_Demo_IAR_ARM7/uip/tapdev.o \
./Demo/uIP_Demo_IAR_ARM7/uip/telnetd-shell.o \
./Demo/uIP_Demo_IAR_ARM7/uip/telnetd.o \
./Demo/uIP_Demo_IAR_ARM7/uip/uip.o \
./Demo/uIP_Demo_IAR_ARM7/uip/uip_arch.o \
./Demo/uIP_Demo_IAR_ARM7/uip/uip_arp.o 

ASM_DEPS += \
./Demo/uIP_Demo_IAR_ARM7/uip/crt0.d 

C_DEPS += \
./Demo/uIP_Demo_IAR_ARM7/uip/cgi.d \
./Demo/uIP_Demo_IAR_ARM7/uip/fs.d \
./Demo/uIP_Demo_IAR_ARM7/uip/fsdata.d \
./Demo/uIP_Demo_IAR_ARM7/uip/httpd.d \
./Demo/uIP_Demo_IAR_ARM7/uip/memb.d \
./Demo/uIP_Demo_IAR_ARM7/uip/slipdev.d \
./Demo/uIP_Demo_IAR_ARM7/uip/tapdev.d \
./Demo/uIP_Demo_IAR_ARM7/uip/telnetd-shell.d \
./Demo/uIP_Demo_IAR_ARM7/uip/telnetd.d \
./Demo/uIP_Demo_IAR_ARM7/uip/uip.d \
./Demo/uIP_Demo_IAR_ARM7/uip/uip_arch.d \
./Demo/uIP_Demo_IAR_ARM7/uip/uip_arp.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/uIP_Demo_IAR_ARM7/uip/%.o: ../Demo/uIP_Demo_IAR_ARM7/uip/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/uIP_Demo_IAR_ARM7/uip/%.o: ../Demo/uIP_Demo_IAR_ARM7/uip/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


