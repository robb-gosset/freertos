################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/LEDs.c \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/main.c \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/main_blinky.c \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/main_full.c \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/printf-stdarg.c \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/serial.c 

S_UPPER_SRCS += \
../Demo/CORTEX_A9_Cyclone_V_SoC_DK/reg_test.S 

OBJS += \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/LEDs.o \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/main.o \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/main_blinky.o \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/main_full.o \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/printf-stdarg.o \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/reg_test.o \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/serial.o 

S_UPPER_DEPS += \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/reg_test.d 

C_DEPS += \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/LEDs.d \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/main.d \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/main_blinky.d \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/main_full.d \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/printf-stdarg.d \
./Demo/CORTEX_A9_Cyclone_V_SoC_DK/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_A9_Cyclone_V_SoC_DK/%.o: ../Demo/CORTEX_A9_Cyclone_V_SoC_DK/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_A9_Cyclone_V_SoC_DK/%.o: ../Demo/CORTEX_A9_Cyclone_V_SoC_DK/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


