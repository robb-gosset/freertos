################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1100.c \
../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1200.c \
../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1300.c \
../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main-blinky.c \
../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main-full.c \
../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main.c 

OBJS += \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1100.o \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1200.o \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1300.o \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main-blinky.o \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main-full.o \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main.o 

C_DEPS += \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1100.d \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1200.d \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/ParTest_XMC1300.d \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main-blinky.d \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main-full.d \
./Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/%.o: ../Demo/CORTEX_M0_Infineon_XMC1000_IAR_Keil_GCC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


