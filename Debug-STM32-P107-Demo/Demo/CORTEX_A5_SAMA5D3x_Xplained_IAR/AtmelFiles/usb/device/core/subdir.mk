################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBD.c \
../Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDCallbacks.c \
../Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDDriver.c \
../Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDDriverCallbacks.c 

OBJS += \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBD.o \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDCallbacks.o \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDDriver.o \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDDriverCallbacks.o 

C_DEPS += \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBD.d \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDCallbacks.d \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDDriver.d \
./Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/USBDDriverCallbacks.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/%.o: ../Demo/CORTEX_A5_SAMA5D3x_Xplained_IAR/AtmelFiles/usb/device/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


