################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/LEDs.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/main.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/printf-stdarg.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/serial.c 

OBJS += \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/LEDs.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/main.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/printf-stdarg.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/serial.o 

C_DEPS += \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/LEDs.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/main.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/printf-stdarg.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/%.o: ../Demo/MicroBlaze_Kintex7_EthernetLite/RTOSDemo/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


