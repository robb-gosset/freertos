################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_extra.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_g.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_intr.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_selftest.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_sinit.c 

OBJS += \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_extra.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_g.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_intr.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_selftest.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_sinit.o 

C_DEPS += \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_extra.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_g.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_intr.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_selftest.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/xgpio_sinit.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/%.o: ../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/gpio_v4_0/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


