################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_g.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_intr.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_l.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_selftest.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_sinit.c \
../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_stats.c 

OBJS += \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_g.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_intr.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_l.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_selftest.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_sinit.o \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_stats.o 

C_DEPS += \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_g.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_intr.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_l.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_selftest.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_sinit.d \
./Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/xuartlite_stats.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/%.o: ../Demo/MicroBlaze_Kintex7_EthernetLite/BSP/microblaze_0/libsrc/uartlite_v3_0/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


