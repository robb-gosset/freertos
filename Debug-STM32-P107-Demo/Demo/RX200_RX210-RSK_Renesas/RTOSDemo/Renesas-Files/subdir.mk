################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/dbsct.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/hd44780.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/hwsetup.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/intprg.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/lcd.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/lowsrc.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/resetprg.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/sbrk.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/vecttbl.c 

OBJS += \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/dbsct.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/hd44780.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/hwsetup.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/intprg.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/lcd.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/lowsrc.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/resetprg.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/sbrk.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/vecttbl.o 

C_DEPS += \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/dbsct.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/hd44780.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/hwsetup.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/intprg.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/lcd.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/lowsrc.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/resetprg.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/sbrk.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/vecttbl.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/%.o: ../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/Renesas-Files/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


