################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/ButtonAndLCD.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/HighFrequencyTimerTest.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/IntQueueTimer.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/ParTest.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/main-blinky.c \
../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/main-full.c 

OBJS += \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/ButtonAndLCD.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/HighFrequencyTimerTest.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/IntQueueTimer.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/ParTest.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/main-blinky.o \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/main-full.o 

C_DEPS += \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/ButtonAndLCD.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/HighFrequencyTimerTest.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/IntQueueTimer.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/ParTest.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/main-blinky.d \
./Demo/RX200_RX210-RSK_Renesas/RTOSDemo/main-full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX200_RX210-RSK_Renesas/RTOSDemo/%.o: ../Demo/RX200_RX210-RSK_Renesas/RTOSDemo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


