################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/dbsct.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/flash_options.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/hwsetup.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/lowsrc.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/resetprg.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/sbrk.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/vecttbl.c 

OBJS += \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/dbsct.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/flash_options.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/hwsetup.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/lowsrc.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/resetprg.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/sbrk.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/vecttbl.o 

C_DEPS += \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/dbsct.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/flash_options.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/hwsetup.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/lowsrc.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/resetprg.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/sbrk.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/vecttbl.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/%.o: ../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/Renesas-Files/board/rdkrx63n/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


