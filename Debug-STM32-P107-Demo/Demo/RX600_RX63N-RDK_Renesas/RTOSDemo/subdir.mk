################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/HighFrequencyTimerTest.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/IntQueueTimer.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/ParTest.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/main-blinky.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/main-full.c \
../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/uIP_Task.c 

OBJS += \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/HighFrequencyTimerTest.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/IntQueueTimer.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/ParTest.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/main-blinky.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/main-full.o \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/uIP_Task.o 

C_DEPS += \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/HighFrequencyTimerTest.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/IntQueueTimer.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/ParTest.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/main-blinky.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/main-full.d \
./Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/%.o: ../Demo/RX600_RX63N-RDK_Renesas/RTOSDemo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


