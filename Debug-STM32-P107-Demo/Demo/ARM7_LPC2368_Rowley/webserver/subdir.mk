################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ARM7_LPC2368_Rowley/webserver/EMAC_ISR.c \
../Demo/ARM7_LPC2368_Rowley/webserver/emac.c \
../Demo/ARM7_LPC2368_Rowley/webserver/http-strings.c \
../Demo/ARM7_LPC2368_Rowley/webserver/httpd-cgi.c \
../Demo/ARM7_LPC2368_Rowley/webserver/httpd-fs.c \
../Demo/ARM7_LPC2368_Rowley/webserver/httpd-fsdata.c \
../Demo/ARM7_LPC2368_Rowley/webserver/httpd.c \
../Demo/ARM7_LPC2368_Rowley/webserver/uIP_Task.c 

OBJS += \
./Demo/ARM7_LPC2368_Rowley/webserver/EMAC_ISR.o \
./Demo/ARM7_LPC2368_Rowley/webserver/emac.o \
./Demo/ARM7_LPC2368_Rowley/webserver/http-strings.o \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd-cgi.o \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd-fs.o \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd-fsdata.o \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd.o \
./Demo/ARM7_LPC2368_Rowley/webserver/uIP_Task.o 

C_DEPS += \
./Demo/ARM7_LPC2368_Rowley/webserver/EMAC_ISR.d \
./Demo/ARM7_LPC2368_Rowley/webserver/emac.d \
./Demo/ARM7_LPC2368_Rowley/webserver/http-strings.d \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd-cgi.d \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd-fs.d \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd-fsdata.d \
./Demo/ARM7_LPC2368_Rowley/webserver/httpd.d \
./Demo/ARM7_LPC2368_Rowley/webserver/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ARM7_LPC2368_Rowley/webserver/%.o: ../Demo/ARM7_LPC2368_Rowley/webserver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


