################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/Minimal/AltBlckQ.c \
../Demo/Common/Minimal/AltBlock.c \
../Demo/Common/Minimal/AltPollQ.c \
../Demo/Common/Minimal/AltQTest.c \
../Demo/Common/Minimal/GenQTest.c \
../Demo/Common/Minimal/IntSemTest.c \
../Demo/Common/Minimal/QPeek.c \
../Demo/Common/Minimal/QueueOverwrite.c \
../Demo/Common/Minimal/QueueSet.c \
../Demo/Common/Minimal/QueueSetPolling.c \
../Demo/Common/Minimal/TaskNotify.c \
../Demo/Common/Minimal/blocktim.c \
../Demo/Common/Minimal/countsem.c \
../Demo/Common/Minimal/crflash.c \
../Demo/Common/Minimal/flash_timer.c \
../Demo/Common/Minimal/recmutex.c 

OBJS += \
./Demo/Common/Minimal/AltBlckQ.o \
./Demo/Common/Minimal/AltBlock.o \
./Demo/Common/Minimal/AltPollQ.o \
./Demo/Common/Minimal/AltQTest.o \
./Demo/Common/Minimal/GenQTest.o \
./Demo/Common/Minimal/IntSemTest.o \
./Demo/Common/Minimal/QPeek.o \
./Demo/Common/Minimal/QueueOverwrite.o \
./Demo/Common/Minimal/QueueSet.o \
./Demo/Common/Minimal/QueueSetPolling.o \
./Demo/Common/Minimal/TaskNotify.o \
./Demo/Common/Minimal/blocktim.o \
./Demo/Common/Minimal/countsem.o \
./Demo/Common/Minimal/crflash.o \
./Demo/Common/Minimal/flash_timer.o \
./Demo/Common/Minimal/recmutex.o 

C_DEPS += \
./Demo/Common/Minimal/AltBlckQ.d \
./Demo/Common/Minimal/AltBlock.d \
./Demo/Common/Minimal/AltPollQ.d \
./Demo/Common/Minimal/AltQTest.d \
./Demo/Common/Minimal/GenQTest.d \
./Demo/Common/Minimal/IntSemTest.d \
./Demo/Common/Minimal/QPeek.d \
./Demo/Common/Minimal/QueueOverwrite.d \
./Demo/Common/Minimal/QueueSet.d \
./Demo/Common/Minimal/QueueSetPolling.d \
./Demo/Common/Minimal/TaskNotify.d \
./Demo/Common/Minimal/blocktim.d \
./Demo/Common/Minimal/countsem.d \
./Demo/Common/Minimal/crflash.d \
./Demo/Common/Minimal/flash_timer.d \
./Demo/Common/Minimal/recmutex.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/Minimal/%.o: ../Demo/Common/Minimal/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


