################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/Full/BlockQ.c \
../Demo/Common/Full/PollQ.c \
../Demo/Common/Full/comtest.c \
../Demo/Common/Full/death.c \
../Demo/Common/Full/dynamic.c \
../Demo/Common/Full/events.c \
../Demo/Common/Full/flash.c \
../Demo/Common/Full/flop.c \
../Demo/Common/Full/integer.c \
../Demo/Common/Full/print.c \
../Demo/Common/Full/semtest.c 

OBJS += \
./Demo/Common/Full/BlockQ.o \
./Demo/Common/Full/PollQ.o \
./Demo/Common/Full/comtest.o \
./Demo/Common/Full/death.o \
./Demo/Common/Full/dynamic.o \
./Demo/Common/Full/events.o \
./Demo/Common/Full/flash.o \
./Demo/Common/Full/flop.o \
./Demo/Common/Full/integer.o \
./Demo/Common/Full/print.o \
./Demo/Common/Full/semtest.o 

C_DEPS += \
./Demo/Common/Full/BlockQ.d \
./Demo/Common/Full/PollQ.d \
./Demo/Common/Full/comtest.d \
./Demo/Common/Full/death.d \
./Demo/Common/Full/dynamic.d \
./Demo/Common/Full/events.d \
./Demo/Common/Full/flash.d \
./Demo/Common/Full/flop.d \
./Demo/Common/Full/integer.d \
./Demo/Common/Full/print.d \
./Demo/Common/Full/semtest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/Full/%.o: ../Demo/Common/Full/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


