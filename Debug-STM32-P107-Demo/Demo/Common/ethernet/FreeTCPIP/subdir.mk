################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/FreeTCPIP/psock.c \
../Demo/Common/ethernet/FreeTCPIP/timer.c \
../Demo/Common/ethernet/FreeTCPIP/uip.c \
../Demo/Common/ethernet/FreeTCPIP/uip_arp.c 

OBJS += \
./Demo/Common/ethernet/FreeTCPIP/psock.o \
./Demo/Common/ethernet/FreeTCPIP/timer.o \
./Demo/Common/ethernet/FreeTCPIP/uip.o \
./Demo/Common/ethernet/FreeTCPIP/uip_arp.o 

C_DEPS += \
./Demo/Common/ethernet/FreeTCPIP/psock.d \
./Demo/Common/ethernet/FreeTCPIP/timer.d \
./Demo/Common/ethernet/FreeTCPIP/uip.d \
./Demo/Common/ethernet/FreeTCPIP/uip_arp.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/FreeTCPIP/%.o: ../Demo/Common/ethernet/FreeTCPIP/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


