################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/uIP/uip-1.0/uip/psock.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/timer.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/uip-fw.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/uip-neighbor.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/uip-split.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/uip.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/uip_arp.c \
../Demo/Common/ethernet/uIP/uip-1.0/uip/uiplib.c 

OBJS += \
./Demo/Common/ethernet/uIP/uip-1.0/uip/psock.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/timer.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip-fw.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip-neighbor.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip-split.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip_arp.o \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uiplib.o 

C_DEPS += \
./Demo/Common/ethernet/uIP/uip-1.0/uip/psock.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/timer.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip-fw.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip-neighbor.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip-split.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uip_arp.d \
./Demo/Common/ethernet/uIP/uip-1.0/uip/uiplib.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/uIP/uip-1.0/uip/%.o: ../Demo/Common/ethernet/uIP/uip-1.0/uip/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


