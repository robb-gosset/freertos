################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/FreeRTOS-uIP/psock.c \
../Demo/Common/ethernet/FreeRTOS-uIP/timer.c \
../Demo/Common/ethernet/FreeRTOS-uIP/uip-split.c \
../Demo/Common/ethernet/FreeRTOS-uIP/uip.c \
../Demo/Common/ethernet/FreeRTOS-uIP/uip_arp.c \
../Demo/Common/ethernet/FreeRTOS-uIP/uiplib.c 

OBJS += \
./Demo/Common/ethernet/FreeRTOS-uIP/psock.o \
./Demo/Common/ethernet/FreeRTOS-uIP/timer.o \
./Demo/Common/ethernet/FreeRTOS-uIP/uip-split.o \
./Demo/Common/ethernet/FreeRTOS-uIP/uip.o \
./Demo/Common/ethernet/FreeRTOS-uIP/uip_arp.o \
./Demo/Common/ethernet/FreeRTOS-uIP/uiplib.o 

C_DEPS += \
./Demo/Common/ethernet/FreeRTOS-uIP/psock.d \
./Demo/Common/ethernet/FreeRTOS-uIP/timer.d \
./Demo/Common/ethernet/FreeRTOS-uIP/uip-split.d \
./Demo/Common/ethernet/FreeRTOS-uIP/uip.d \
./Demo/Common/ethernet/FreeRTOS-uIP/uip_arp.d \
./Demo/Common/ethernet/FreeRTOS-uIP/uiplib.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/FreeRTOS-uIP/%.o: ../Demo/Common/ethernet/FreeRTOS-uIP/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


