################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/lwIP_130/src/core/dhcp.c \
../Demo/Common/ethernet/lwIP_130/src/core/dns.c \
../Demo/Common/ethernet/lwIP_130/src/core/init.c \
../Demo/Common/ethernet/lwIP_130/src/core/mem.c \
../Demo/Common/ethernet/lwIP_130/src/core/memp.c \
../Demo/Common/ethernet/lwIP_130/src/core/netif.c \
../Demo/Common/ethernet/lwIP_130/src/core/pbuf.c \
../Demo/Common/ethernet/lwIP_130/src/core/raw.c \
../Demo/Common/ethernet/lwIP_130/src/core/stats.c \
../Demo/Common/ethernet/lwIP_130/src/core/sys.c \
../Demo/Common/ethernet/lwIP_130/src/core/tcp.c \
../Demo/Common/ethernet/lwIP_130/src/core/tcp_in.c \
../Demo/Common/ethernet/lwIP_130/src/core/tcp_out.c \
../Demo/Common/ethernet/lwIP_130/src/core/udp.c 

OBJS += \
./Demo/Common/ethernet/lwIP_130/src/core/dhcp.o \
./Demo/Common/ethernet/lwIP_130/src/core/dns.o \
./Demo/Common/ethernet/lwIP_130/src/core/init.o \
./Demo/Common/ethernet/lwIP_130/src/core/mem.o \
./Demo/Common/ethernet/lwIP_130/src/core/memp.o \
./Demo/Common/ethernet/lwIP_130/src/core/netif.o \
./Demo/Common/ethernet/lwIP_130/src/core/pbuf.o \
./Demo/Common/ethernet/lwIP_130/src/core/raw.o \
./Demo/Common/ethernet/lwIP_130/src/core/stats.o \
./Demo/Common/ethernet/lwIP_130/src/core/sys.o \
./Demo/Common/ethernet/lwIP_130/src/core/tcp.o \
./Demo/Common/ethernet/lwIP_130/src/core/tcp_in.o \
./Demo/Common/ethernet/lwIP_130/src/core/tcp_out.o \
./Demo/Common/ethernet/lwIP_130/src/core/udp.o 

C_DEPS += \
./Demo/Common/ethernet/lwIP_130/src/core/dhcp.d \
./Demo/Common/ethernet/lwIP_130/src/core/dns.d \
./Demo/Common/ethernet/lwIP_130/src/core/init.d \
./Demo/Common/ethernet/lwIP_130/src/core/mem.d \
./Demo/Common/ethernet/lwIP_130/src/core/memp.d \
./Demo/Common/ethernet/lwIP_130/src/core/netif.d \
./Demo/Common/ethernet/lwIP_130/src/core/pbuf.d \
./Demo/Common/ethernet/lwIP_130/src/core/raw.d \
./Demo/Common/ethernet/lwIP_130/src/core/stats.d \
./Demo/Common/ethernet/lwIP_130/src/core/sys.d \
./Demo/Common/ethernet/lwIP_130/src/core/tcp.d \
./Demo/Common/ethernet/lwIP_130/src/core/tcp_in.d \
./Demo/Common/ethernet/lwIP_130/src/core/tcp_out.d \
./Demo/Common/ethernet/lwIP_130/src/core/udp.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/lwIP_130/src/core/%.o: ../Demo/Common/ethernet/lwIP_130/src/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


