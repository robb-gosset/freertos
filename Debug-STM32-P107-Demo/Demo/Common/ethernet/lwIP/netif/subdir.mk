################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/lwIP/netif/etharp.c \
../Demo/Common/ethernet/lwIP/netif/ethernetif.c \
../Demo/Common/ethernet/lwIP/netif/loopif.c \
../Demo/Common/ethernet/lwIP/netif/slipif.c 

OBJS += \
./Demo/Common/ethernet/lwIP/netif/etharp.o \
./Demo/Common/ethernet/lwIP/netif/ethernetif.o \
./Demo/Common/ethernet/lwIP/netif/loopif.o \
./Demo/Common/ethernet/lwIP/netif/slipif.o 

C_DEPS += \
./Demo/Common/ethernet/lwIP/netif/etharp.d \
./Demo/Common/ethernet/lwIP/netif/ethernetif.d \
./Demo/Common/ethernet/lwIP/netif/loopif.d \
./Demo/Common/ethernet/lwIP/netif/slipif.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/lwIP/netif/%.o: ../Demo/Common/ethernet/lwIP/netif/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


