################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/lwIP/core/dhcp.c \
../Demo/Common/ethernet/lwIP/core/inet.c \
../Demo/Common/ethernet/lwIP/core/inet6.c \
../Demo/Common/ethernet/lwIP/core/mem.c \
../Demo/Common/ethernet/lwIP/core/memp.c \
../Demo/Common/ethernet/lwIP/core/netif.c \
../Demo/Common/ethernet/lwIP/core/pbuf.c \
../Demo/Common/ethernet/lwIP/core/raw.c \
../Demo/Common/ethernet/lwIP/core/stats.c \
../Demo/Common/ethernet/lwIP/core/sys.c \
../Demo/Common/ethernet/lwIP/core/tcp.c \
../Demo/Common/ethernet/lwIP/core/tcp_in.c \
../Demo/Common/ethernet/lwIP/core/tcp_out.c \
../Demo/Common/ethernet/lwIP/core/udp.c 

OBJS += \
./Demo/Common/ethernet/lwIP/core/dhcp.o \
./Demo/Common/ethernet/lwIP/core/inet.o \
./Demo/Common/ethernet/lwIP/core/inet6.o \
./Demo/Common/ethernet/lwIP/core/mem.o \
./Demo/Common/ethernet/lwIP/core/memp.o \
./Demo/Common/ethernet/lwIP/core/netif.o \
./Demo/Common/ethernet/lwIP/core/pbuf.o \
./Demo/Common/ethernet/lwIP/core/raw.o \
./Demo/Common/ethernet/lwIP/core/stats.o \
./Demo/Common/ethernet/lwIP/core/sys.o \
./Demo/Common/ethernet/lwIP/core/tcp.o \
./Demo/Common/ethernet/lwIP/core/tcp_in.o \
./Demo/Common/ethernet/lwIP/core/tcp_out.o \
./Demo/Common/ethernet/lwIP/core/udp.o 

C_DEPS += \
./Demo/Common/ethernet/lwIP/core/dhcp.d \
./Demo/Common/ethernet/lwIP/core/inet.d \
./Demo/Common/ethernet/lwIP/core/inet6.d \
./Demo/Common/ethernet/lwIP/core/mem.d \
./Demo/Common/ethernet/lwIP/core/memp.d \
./Demo/Common/ethernet/lwIP/core/netif.d \
./Demo/Common/ethernet/lwIP/core/pbuf.d \
./Demo/Common/ethernet/lwIP/core/raw.d \
./Demo/Common/ethernet/lwIP/core/stats.d \
./Demo/Common/ethernet/lwIP/core/sys.d \
./Demo/Common/ethernet/lwIP/core/tcp.d \
./Demo/Common/ethernet/lwIP/core/tcp_in.d \
./Demo/Common/ethernet/lwIP/core/tcp_out.d \
./Demo/Common/ethernet/lwIP/core/udp.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/lwIP/core/%.o: ../Demo/Common/ethernet/lwIP/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


