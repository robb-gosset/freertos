################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/lwIP/api/api_lib.c \
../Demo/Common/ethernet/lwIP/api/api_msg.c \
../Demo/Common/ethernet/lwIP/api/err.c \
../Demo/Common/ethernet/lwIP/api/sockets.c \
../Demo/Common/ethernet/lwIP/api/tcpip.c 

OBJS += \
./Demo/Common/ethernet/lwIP/api/api_lib.o \
./Demo/Common/ethernet/lwIP/api/api_msg.o \
./Demo/Common/ethernet/lwIP/api/err.o \
./Demo/Common/ethernet/lwIP/api/sockets.o \
./Demo/Common/ethernet/lwIP/api/tcpip.o 

C_DEPS += \
./Demo/Common/ethernet/lwIP/api/api_lib.d \
./Demo/Common/ethernet/lwIP/api/api_msg.d \
./Demo/Common/ethernet/lwIP/api/err.d \
./Demo/Common/ethernet/lwIP/api/sockets.d \
./Demo/Common/ethernet/lwIP/api/tcpip.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/lwIP/api/%.o: ../Demo/Common/ethernet/lwIP/api/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


