################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/autoip.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/icmp.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/igmp.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/inet.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/inet_chksum.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip_addr.c \
../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip_frag.c 

OBJS += \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/autoip.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/icmp.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/igmp.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/inet.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/inet_chksum.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip_addr.o \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip_frag.o 

C_DEPS += \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/autoip.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/icmp.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/igmp.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/inet.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/inet_chksum.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip_addr.d \
./Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/ip_frag.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/%.o: ../Demo/Common/ethernet/lwip-1.4.0/src/core/ipv4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


