################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_dbgmcu.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_gpio.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_lib.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_nvic.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_rcc.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_spi.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_systick.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_tim.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_usart.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32fxxx_eth.c \
../Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32fxxx_eth_lib.c 

OBJS += \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_dbgmcu.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_gpio.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_lib.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_nvic.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_rcc.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_spi.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_systick.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_tim.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_usart.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32fxxx_eth.o \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32fxxx_eth_lib.o 

C_DEPS += \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_dbgmcu.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_gpio.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_lib.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_nvic.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_rcc.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_spi.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_systick.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_tim.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32f10x_usart.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32fxxx_eth.d \
./Demo/Common/drivers/ST/STM32F10xFWLib/src/stm32fxxx_eth_lib.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/Common/drivers/ST/STM32F10xFWLib/src/%.o: ../Demo/Common/drivers/ST/STM32F10xFWLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


