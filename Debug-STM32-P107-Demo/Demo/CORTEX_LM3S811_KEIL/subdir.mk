################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LM3S811_KEIL/main.c 

S_UPPER_SRCS += \
../Demo/CORTEX_LM3S811_KEIL/startup_rvmdk.S 

OBJS += \
./Demo/CORTEX_LM3S811_KEIL/main.o \
./Demo/CORTEX_LM3S811_KEIL/startup_rvmdk.o 

S_UPPER_DEPS += \
./Demo/CORTEX_LM3S811_KEIL/startup_rvmdk.d 

C_DEPS += \
./Demo/CORTEX_LM3S811_KEIL/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LM3S811_KEIL/%.o: ../Demo/CORTEX_LM3S811_KEIL/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_LM3S811_KEIL/%.o: ../Demo/CORTEX_LM3S811_KEIL/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


