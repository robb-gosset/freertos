################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/IntQueueTimer.c \
../Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/ParTest.c \
../Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/Serial.c \
../Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/TimerTest.c \
../Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/main.c 

OBJS += \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/IntQueueTimer.o \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/ParTest.o \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/Serial.o \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/TimerTest.o \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/main.o 

C_DEPS += \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/IntQueueTimer.d \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/ParTest.d \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/Serial.d \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/TimerTest.d \
./Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/%.o: ../Demo/CORTEX_CY8C5588_PSoC_Creator_RVDS/FreeRTOS_Demo.cydsn/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


