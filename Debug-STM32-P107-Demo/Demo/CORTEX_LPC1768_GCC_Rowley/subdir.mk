################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LPC1768_GCC_Rowley/ParTest.c \
../Demo/CORTEX_LPC1768_GCC_Rowley/main.c \
../Demo/CORTEX_LPC1768_GCC_Rowley/printf-stdarg.c 

OBJS += \
./Demo/CORTEX_LPC1768_GCC_Rowley/ParTest.o \
./Demo/CORTEX_LPC1768_GCC_Rowley/main.o \
./Demo/CORTEX_LPC1768_GCC_Rowley/printf-stdarg.o 

C_DEPS += \
./Demo/CORTEX_LPC1768_GCC_Rowley/ParTest.d \
./Demo/CORTEX_LPC1768_GCC_Rowley/main.d \
./Demo/CORTEX_LPC1768_GCC_Rowley/printf-stdarg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LPC1768_GCC_Rowley/%.o: ../Demo/CORTEX_LPC1768_GCC_Rowley/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


