################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX600_RX62N-RSK_IAR/HighFrequencyTimerTest.c \
../Demo/RX600_RX62N-RSK_IAR/IntQueueTimer.c \
../Demo/RX600_RX62N-RSK_IAR/ParTest.c \
../Demo/RX600_RX62N-RSK_IAR/RSKRX62N_Demo.c \
../Demo/RX600_RX62N-RSK_IAR/main-blinky.c \
../Demo/RX600_RX62N-RSK_IAR/main-full.c \
../Demo/RX600_RX62N-RSK_IAR/uIP_Task.c 

OBJS += \
./Demo/RX600_RX62N-RSK_IAR/HighFrequencyTimerTest.o \
./Demo/RX600_RX62N-RSK_IAR/IntQueueTimer.o \
./Demo/RX600_RX62N-RSK_IAR/ParTest.o \
./Demo/RX600_RX62N-RSK_IAR/RSKRX62N_Demo.o \
./Demo/RX600_RX62N-RSK_IAR/main-blinky.o \
./Demo/RX600_RX62N-RSK_IAR/main-full.o \
./Demo/RX600_RX62N-RSK_IAR/uIP_Task.o 

C_DEPS += \
./Demo/RX600_RX62N-RSK_IAR/HighFrequencyTimerTest.d \
./Demo/RX600_RX62N-RSK_IAR/IntQueueTimer.d \
./Demo/RX600_RX62N-RSK_IAR/ParTest.d \
./Demo/RX600_RX62N-RSK_IAR/RSKRX62N_Demo.d \
./Demo/RX600_RX62N-RSK_IAR/main-blinky.d \
./Demo/RX600_RX62N-RSK_IAR/main-full.d \
./Demo/RX600_RX62N-RSK_IAR/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX600_RX62N-RSK_IAR/%.o: ../Demo/RX600_RX62N-RSK_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


