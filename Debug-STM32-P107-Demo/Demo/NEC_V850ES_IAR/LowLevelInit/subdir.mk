################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit.c \
../Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit_Fx3.c \
../Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit_Hx2.c 

OBJS += \
./Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit.o \
./Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit_Fx3.o \
./Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit_Hx2.o 

C_DEPS += \
./Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit.d \
./Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit_Fx3.d \
./Demo/NEC_V850ES_IAR/LowLevelInit/LowLevelInit_Hx2.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/NEC_V850ES_IAR/LowLevelInit/%.o: ../Demo/NEC_V850ES_IAR/LowLevelInit/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


