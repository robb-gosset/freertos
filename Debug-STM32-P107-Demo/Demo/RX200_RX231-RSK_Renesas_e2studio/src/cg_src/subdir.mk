################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_cgc.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_cgc_user.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_dbsct.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_hardware_setup.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_icu.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_intprg.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_main.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_port.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_resetprg.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_sbrk.c \
../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_vecttbl.c 

OBJS += \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_cgc.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_cgc_user.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_dbsct.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_hardware_setup.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_icu.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_intprg.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_main.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_port.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_resetprg.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_sbrk.o \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_vecttbl.o 

C_DEPS += \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_cgc.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_cgc_user.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_dbsct.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_hardware_setup.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_icu.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_intprg.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_main.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_port.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_resetprg.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_sbrk.d \
./Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/r_cg_vecttbl.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/%.o: ../Demo/RX200_RX231-RSK_Renesas_e2studio/src/cg_src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


