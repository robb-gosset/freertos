################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32L152_Discovery_IAR/STM32L_low_power_tick_management.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/main.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/main_full.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/main_low_power.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/stm32l1xx_it.c 

OBJS += \
./Demo/CORTEX_STM32L152_Discovery_IAR/STM32L_low_power_tick_management.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/main.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/main_full.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/main_low_power.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/stm32l1xx_it.o 

C_DEPS += \
./Demo/CORTEX_STM32L152_Discovery_IAR/STM32L_low_power_tick_management.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/main.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/main_full.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/main_low_power.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/stm32l1xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32L152_Discovery_IAR/%.o: ../Demo/CORTEX_STM32L152_Discovery_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


