################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/misc.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_exti.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_gpio.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_lcd.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_pwr.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rcc.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rtc.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_syscfg.c \
../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_tim.c 

OBJS += \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/misc.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_exti.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_gpio.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_lcd.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_pwr.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rcc.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rtc.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_syscfg.o \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_tim.o 

C_DEPS += \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/misc.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_exti.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_gpio.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_lcd.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_pwr.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rcc.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rtc.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_syscfg.d \
./Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_tim.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/%.o: ../Demo/CORTEX_STM32L152_Discovery_IAR/ST_Code/Libraries/STM32L1xx_StdPeriph_Driver/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


