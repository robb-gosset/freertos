################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RegTest.asm 

C_SRCS += \
../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/IntQueueTimer.c \
../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RegTest.c \
../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RunTimeStatsTimer.c \
../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/main_full.c \
../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/serial.c 

OBJS += \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/IntQueueTimer.o \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RegTest.o \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RunTimeStatsTimer.o \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/main_full.o \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/serial.o 

ASM_DEPS += \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RegTest.d 

C_DEPS += \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/IntQueueTimer.d \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RegTest.d \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/RunTimeStatsTimer.d \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/main_full.d \
./Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/%.o: ../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/%.o: ../Demo/CORTEX_M4F_MSP432_LaunchPad_IAR_CCS_Keil/Full_Demo/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


