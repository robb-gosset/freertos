################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F103_IAR/main.c \
../Demo/CORTEX_STM32F103_IAR/spi_flash.c \
../Demo/CORTEX_STM32F103_IAR/stm32f10x_it.c \
../Demo/CORTEX_STM32F103_IAR/stm32f10x_vector.c \
../Demo/CORTEX_STM32F103_IAR/timertest.c 

OBJS += \
./Demo/CORTEX_STM32F103_IAR/main.o \
./Demo/CORTEX_STM32F103_IAR/spi_flash.o \
./Demo/CORTEX_STM32F103_IAR/stm32f10x_it.o \
./Demo/CORTEX_STM32F103_IAR/stm32f10x_vector.o \
./Demo/CORTEX_STM32F103_IAR/timertest.o 

C_DEPS += \
./Demo/CORTEX_STM32F103_IAR/main.d \
./Demo/CORTEX_STM32F103_IAR/spi_flash.d \
./Demo/CORTEX_STM32F103_IAR/stm32f10x_it.d \
./Demo/CORTEX_STM32F103_IAR/stm32f10x_vector.d \
./Demo/CORTEX_STM32F103_IAR/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F103_IAR/%.o: ../Demo/CORTEX_STM32F103_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


