################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/lcd.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_adc.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_bkp.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_can.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_dma.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_exti.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_gpio.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_i2c.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_iwdg.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_lib.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_nvic.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_pwr.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_rcc.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_rtc.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_spi.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_systick.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_tim.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_tim1.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_usart.c \
../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_wwdg.c 

OBJS += \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/lcd.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_adc.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_bkp.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_can.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_dma.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_exti.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_gpio.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_i2c.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_iwdg.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_lib.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_nvic.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_pwr.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_rcc.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_rtc.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_spi.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_systick.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_tim.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_tim1.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_usart.o \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_wwdg.o 

C_DEPS += \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/lcd.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_adc.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_bkp.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_can.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_dma.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_exti.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_gpio.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_i2c.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_iwdg.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_lib.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_nvic.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_pwr.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_rcc.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_rtc.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_spi.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_systick.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_tim.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_tim1.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_usart.d \
./Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/stm32f10x_wwdg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/%.o: ../Demo/CORTEX_STM32F103_IAR/STM32F10xFWLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


