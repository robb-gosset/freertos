################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MX_MPLAB/ConfigPerformance.c \
../Demo/PIC32MX_MPLAB/IntQueueTimer.c \
../Demo/PIC32MX_MPLAB/lcd.c \
../Demo/PIC32MX_MPLAB/main.c \
../Demo/PIC32MX_MPLAB/main_blinky.c \
../Demo/PIC32MX_MPLAB/main_full.c \
../Demo/PIC32MX_MPLAB/printf-stdarg.c \
../Demo/PIC32MX_MPLAB/timertest.c 

S_UPPER_SRCS += \
../Demo/PIC32MX_MPLAB/IntQueueTimer_isr.S \
../Demo/PIC32MX_MPLAB/RegisterTestTasks.S \
../Demo/PIC32MX_MPLAB/timertest_isr.S 

OBJS += \
./Demo/PIC32MX_MPLAB/ConfigPerformance.o \
./Demo/PIC32MX_MPLAB/IntQueueTimer.o \
./Demo/PIC32MX_MPLAB/IntQueueTimer_isr.o \
./Demo/PIC32MX_MPLAB/RegisterTestTasks.o \
./Demo/PIC32MX_MPLAB/lcd.o \
./Demo/PIC32MX_MPLAB/main.o \
./Demo/PIC32MX_MPLAB/main_blinky.o \
./Demo/PIC32MX_MPLAB/main_full.o \
./Demo/PIC32MX_MPLAB/printf-stdarg.o \
./Demo/PIC32MX_MPLAB/timertest.o \
./Demo/PIC32MX_MPLAB/timertest_isr.o 

S_UPPER_DEPS += \
./Demo/PIC32MX_MPLAB/IntQueueTimer_isr.d \
./Demo/PIC32MX_MPLAB/RegisterTestTasks.d \
./Demo/PIC32MX_MPLAB/timertest_isr.d 

C_DEPS += \
./Demo/PIC32MX_MPLAB/ConfigPerformance.d \
./Demo/PIC32MX_MPLAB/IntQueueTimer.d \
./Demo/PIC32MX_MPLAB/lcd.d \
./Demo/PIC32MX_MPLAB/main.d \
./Demo/PIC32MX_MPLAB/main_blinky.d \
./Demo/PIC32MX_MPLAB/main_full.d \
./Demo/PIC32MX_MPLAB/printf-stdarg.d \
./Demo/PIC32MX_MPLAB/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MX_MPLAB/%.o: ../Demo/PIC32MX_MPLAB/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/PIC32MX_MPLAB/%.o: ../Demo/PIC32MX_MPLAB/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


