################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MX_MPLAB/ParTest/ParTest_Explorer16.c \
../Demo/PIC32MX_MPLAB/ParTest/ParTest_USBII_Starter_Kit.c 

OBJS += \
./Demo/PIC32MX_MPLAB/ParTest/ParTest_Explorer16.o \
./Demo/PIC32MX_MPLAB/ParTest/ParTest_USBII_Starter_Kit.o 

C_DEPS += \
./Demo/PIC32MX_MPLAB/ParTest/ParTest_Explorer16.d \
./Demo/PIC32MX_MPLAB/ParTest/ParTest_USBII_Starter_Kit.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MX_MPLAB/ParTest/%.o: ../Demo/PIC32MX_MPLAB/ParTest/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


