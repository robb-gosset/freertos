################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/default-on-bootstrap.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/on_reset.c 

S_UPPER_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crt0.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crti.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crtn.S 

OBJS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crt0.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crti.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crtn.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/default-on-bootstrap.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/on_reset.o 

S_UPPER_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crt0.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crti.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/crtn.d 

C_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/default-on-bootstrap.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/on_reset.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/startup/MPLAB/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


