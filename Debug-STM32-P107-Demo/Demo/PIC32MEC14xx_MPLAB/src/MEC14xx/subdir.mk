################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_bbled.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_gpio.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_jtvic.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_system.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_tfdp.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_timers.c 

OBJS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_bbled.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_gpio.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_jtvic.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_system.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_tfdp.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_timers.o 

C_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_bbled.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_gpio.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_jtvic.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_system.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_tfdp.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/mec14xx_timers.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


