################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq08.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq09.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq10.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq11.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq12.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq13.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq14.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq15.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq16.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq17.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq18.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq19.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq20.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq21.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq22.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq23.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq24.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq25.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq26.c \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girqs.c 

S_UPPER_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq08d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq09d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq10d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq11d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq12d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq13d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq14d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq15d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq16d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq17d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq18d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq19d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq20d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq21d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq22d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq23d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq24d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq25d.S \
../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq26d.S 

OBJS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq08.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq08d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq09.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq09d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq10.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq10d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq11.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq11d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq12.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq12d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq13.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq13d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq14.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq14d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq15.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq15d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq16.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq16d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq17.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq17d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq18.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq18d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq19.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq19d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq20.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq20d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq21.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq21d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq22.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq22d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq23.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq23d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq24.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq24d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq25.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq25d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq26.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq26d.o \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girqs.o 

S_UPPER_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq08d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq09d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq10d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq11d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq12d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq13d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq14d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq15d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq16d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq17d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq18d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq19d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq20d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq21d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq22d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq23d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq24d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq25d.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq26d.d 

C_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq08.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq09.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq10.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq11.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq12.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq13.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq14.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq15.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq16.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq17.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq18.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq19.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq20.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq21.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq22.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq23.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq24.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq25.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girq26.d \
./Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/girqs.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/MEC14xx/interrupts/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


