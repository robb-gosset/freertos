################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/IntQueueTimer.c \
../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/main_full.c \
../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/timertest.c 

S_UPPER_SRCS += \
../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/IntQueueTimer_isr.S \
../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/RegisterTestTasks.S 

OBJS += \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/IntQueueTimer.o \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/IntQueueTimer_isr.o \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/RegisterTestTasks.o \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/main_full.o \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/timertest.o 

S_UPPER_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/IntQueueTimer_isr.d \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/RegisterTestTasks.d 

C_DEPS += \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/IntQueueTimer.d \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/main_full.d \
./Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/%.o: ../Demo/PIC32MEC14xx_MPLAB/src/Full_Demo/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


