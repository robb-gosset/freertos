################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/mb96348hs.asm 

C_SRCS += \
../Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/crflash_sk16fx100mpc.c \
../Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/main.c \
../Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/vectors.c 

OBJS += \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/crflash_sk16fx100mpc.o \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/main.o \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/mb96348hs.o \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/vectors.o 

ASM_DEPS += \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/mb96348hs.d 

C_DEPS += \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/crflash_sk16fx100mpc.d \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/main.d \
./Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/vectors.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/%.o: ../Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/%.o: ../Demo/MB96340_Softune/FreeRTOS_96348hs_SK16FX100PMC/Src/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


