################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ARM7_STR71x_IAR/Library/71x_it.c \
../Demo/ARM7_STR71x_IAR/Library/71x_lib.c \
../Demo/ARM7_STR71x_IAR/Library/gpio.c \
../Demo/ARM7_STR71x_IAR/Library/rccu.c \
../Demo/ARM7_STR71x_IAR/Library/uart.c \
../Demo/ARM7_STR71x_IAR/Library/wdg.c 

OBJS += \
./Demo/ARM7_STR71x_IAR/Library/71x_it.o \
./Demo/ARM7_STR71x_IAR/Library/71x_lib.o \
./Demo/ARM7_STR71x_IAR/Library/gpio.o \
./Demo/ARM7_STR71x_IAR/Library/rccu.o \
./Demo/ARM7_STR71x_IAR/Library/uart.o \
./Demo/ARM7_STR71x_IAR/Library/wdg.o 

C_DEPS += \
./Demo/ARM7_STR71x_IAR/Library/71x_it.d \
./Demo/ARM7_STR71x_IAR/Library/71x_lib.d \
./Demo/ARM7_STR71x_IAR/Library/gpio.d \
./Demo/ARM7_STR71x_IAR/Library/rccu.d \
./Demo/ARM7_STR71x_IAR/Library/uart.d \
./Demo/ARM7_STR71x_IAR/Library/wdg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ARM7_STR71x_IAR/Library/%.o: ../Demo/ARM7_STR71x_IAR/Library/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


