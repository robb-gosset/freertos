################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_board.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_buttons.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_lcd.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_lcd_fonts.c 

OBJS += \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_board.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_buttons.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_lcd.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_lcd_fonts.o 

C_DEPS += \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_board.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_buttons.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_lcd.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/hal_lcd_fonts.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/%.o: ../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/MSP-EXP430F5438_HAL/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


