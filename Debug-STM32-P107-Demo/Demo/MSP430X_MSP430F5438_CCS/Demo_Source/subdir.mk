################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/RegTest.asm 

C_SRCS += \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/ParTest.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/RunTimeStatsConfig.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/main.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/printf-stdarg.c \
../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/serial.c 

OBJS += \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/ParTest.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/RegTest.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/RunTimeStatsConfig.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/main.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/printf-stdarg.o \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/serial.o 

ASM_DEPS += \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/RegTest.d 

C_DEPS += \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/ParTest.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/RunTimeStatsConfig.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/main.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/printf-stdarg.d \
./Demo/MSP430X_MSP430F5438_CCS/Demo_Source/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MSP430X_MSP430F5438_CCS/Demo_Source/%.o: ../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/MSP430X_MSP430F5438_CCS/Demo_Source/%.o: ../Demo/MSP430X_MSP430F5438_CCS/Demo_Source/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


