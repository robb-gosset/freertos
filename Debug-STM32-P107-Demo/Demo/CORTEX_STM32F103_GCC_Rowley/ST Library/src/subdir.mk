################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_can.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_gpio.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_i2c.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_lib.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_nvic.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_rcc.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_spi.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_systick.c \
../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_usart.c 

OBJS += \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_can.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_gpio.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_i2c.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_lib.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_nvic.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_rcc.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_spi.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_systick.o \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_usart.o 

C_DEPS += \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_can.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_gpio.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_i2c.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_lib.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_nvic.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_rcc.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_spi.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_systick.d \
./Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_usart.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_can.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_can.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_can.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_can.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_gpio.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_gpio.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_gpio.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_gpio.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_i2c.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_i2c.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_i2c.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_i2c.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_lib.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_lib.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_lib.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_lib.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_nvic.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_nvic.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_nvic.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_nvic.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_rcc.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_rcc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_rcc.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_rcc.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_spi.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_spi.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_spi.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_spi.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_systick.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_systick.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_systick.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_systick.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_usart.o: ../Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_usart.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/CORTEX_STM32F103_GCC_Rowley/ST Library/src/stm32f10x_usart.d" -MT"Demo/CORTEX_STM32F103_GCC_Rowley/ST\ Library/src/stm32f10x_usart.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


