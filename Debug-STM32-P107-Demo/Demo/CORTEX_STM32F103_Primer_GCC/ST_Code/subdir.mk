################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/Util.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/buzzer.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/crt0_STM32x.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/draw.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/lcd.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/led.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/mems.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/pointer.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/stm32f10x_circle_it.c \
../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/stm32f10x_tim.c 

OBJS += \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/Util.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/buzzer.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/crt0_STM32x.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/draw.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/lcd.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/led.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/mems.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/pointer.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/stm32f10x_circle_it.o \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/stm32f10x_tim.o 

C_DEPS += \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/Util.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/buzzer.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/crt0_STM32x.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/draw.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/lcd.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/led.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/mems.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/pointer.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/stm32f10x_circle_it.d \
./Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/stm32f10x_tim.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/%.o: ../Demo/CORTEX_STM32F103_Primer_GCC/ST_Code/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


