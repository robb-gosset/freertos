################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F103_Primer_GCC/main.c \
../Demo/CORTEX_STM32F103_Primer_GCC/printf-stdarg.c \
../Demo/CORTEX_STM32F103_Primer_GCC/syscalls.c \
../Demo/CORTEX_STM32F103_Primer_GCC/timertest.c 

OBJS += \
./Demo/CORTEX_STM32F103_Primer_GCC/main.o \
./Demo/CORTEX_STM32F103_Primer_GCC/printf-stdarg.o \
./Demo/CORTEX_STM32F103_Primer_GCC/syscalls.o \
./Demo/CORTEX_STM32F103_Primer_GCC/timertest.o 

C_DEPS += \
./Demo/CORTEX_STM32F103_Primer_GCC/main.d \
./Demo/CORTEX_STM32F103_Primer_GCC/printf-stdarg.d \
./Demo/CORTEX_STM32F103_Primer_GCC/syscalls.d \
./Demo/CORTEX_STM32F103_Primer_GCC/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F103_Primer_GCC/%.o: ../Demo/CORTEX_STM32F103_Primer_GCC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


