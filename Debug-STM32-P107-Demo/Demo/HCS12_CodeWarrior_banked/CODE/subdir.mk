################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_UPPER_SRCS += \
../Demo/HCS12_CodeWarrior_banked/CODE/Byte1.C \
../Demo/HCS12_CodeWarrior_banked/CODE/Cpu.C \
../Demo/HCS12_CodeWarrior_banked/CODE/Events.C \
../Demo/HCS12_CodeWarrior_banked/CODE/IO_Map.C \
../Demo/HCS12_CodeWarrior_banked/CODE/PE_Timer.C \
../Demo/HCS12_CodeWarrior_banked/CODE/RTOSDemo.C \
../Demo/HCS12_CodeWarrior_banked/CODE/TickTimer.C \
../Demo/HCS12_CodeWarrior_banked/CODE/_COM0.C 

C_SRCS += \
../Demo/HCS12_CodeWarrior_banked/CODE/Copy\ of\ Vectors.c \
../Demo/HCS12_CodeWarrior_banked/CODE/Vectors.c 

OBJS += \
./Demo/HCS12_CodeWarrior_banked/CODE/Byte1.o \
./Demo/HCS12_CodeWarrior_banked/CODE/Copy\ of\ Vectors.o \
./Demo/HCS12_CodeWarrior_banked/CODE/Cpu.o \
./Demo/HCS12_CodeWarrior_banked/CODE/Events.o \
./Demo/HCS12_CodeWarrior_banked/CODE/IO_Map.o \
./Demo/HCS12_CodeWarrior_banked/CODE/PE_Timer.o \
./Demo/HCS12_CodeWarrior_banked/CODE/RTOSDemo.o \
./Demo/HCS12_CodeWarrior_banked/CODE/TickTimer.o \
./Demo/HCS12_CodeWarrior_banked/CODE/Vectors.o \
./Demo/HCS12_CodeWarrior_banked/CODE/_COM0.o 

C_UPPER_DEPS += \
./Demo/HCS12_CodeWarrior_banked/CODE/Byte1.d \
./Demo/HCS12_CodeWarrior_banked/CODE/Cpu.d \
./Demo/HCS12_CodeWarrior_banked/CODE/Events.d \
./Demo/HCS12_CodeWarrior_banked/CODE/IO_Map.d \
./Demo/HCS12_CodeWarrior_banked/CODE/PE_Timer.d \
./Demo/HCS12_CodeWarrior_banked/CODE/RTOSDemo.d \
./Demo/HCS12_CodeWarrior_banked/CODE/TickTimer.d \
./Demo/HCS12_CodeWarrior_banked/CODE/_COM0.d 

C_DEPS += \
./Demo/HCS12_CodeWarrior_banked/CODE/Copy\ of\ Vectors.d \
./Demo/HCS12_CodeWarrior_banked/CODE/Vectors.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/HCS12_CodeWarrior_banked/CODE/%.o: ../Demo/HCS12_CodeWarrior_banked/CODE/%.C
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -std=gnu++11 -fabi-version=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/HCS12_CodeWarrior_banked/CODE/Copy\ of\ Vectors.o: ../Demo/HCS12_CodeWarrior_banked/CODE/Copy\ of\ Vectors.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"Demo/HCS12_CodeWarrior_banked/CODE/Copy of Vectors.d" -MT"Demo/HCS12_CodeWarrior_banked/CODE/Copy\ of\ Vectors.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/HCS12_CodeWarrior_banked/CODE/%.o: ../Demo/HCS12_CodeWarrior_banked/CODE/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


