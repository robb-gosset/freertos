################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_cgc.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_cgc_user.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_dbsct.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_hardware_setup.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_icu.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_icu_user.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_intprg.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_port.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_port_user.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_resetprg.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sbrk.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sci.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sci_user.c \
../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_vecttbl.c 

OBJS += \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_cgc.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_cgc_user.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_dbsct.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_hardware_setup.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_icu.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_icu_user.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_intprg.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_port.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_port_user.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_resetprg.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sbrk.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sci.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sci_user.o \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_vecttbl.o 

C_DEPS += \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_cgc.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_cgc_user.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_dbsct.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_hardware_setup.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_icu.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_icu_user.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_intprg.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_port.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_port_user.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_resetprg.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sbrk.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sci.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_sci_user.d \
./Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/r_cg_vecttbl.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/%.o: ../Demo/RX700_RX71M_RSK_Renesas_e2studio/src/cg_src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


