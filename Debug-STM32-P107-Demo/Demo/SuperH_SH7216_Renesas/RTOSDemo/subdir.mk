################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/RTOSDemo.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/dbsct.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/flop.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/intprg.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/main.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/printf-stdarg.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/resetprg.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/uIP_Task.c \
../Demo/SuperH_SH7216_Renesas/RTOSDemo/vecttbl.c 

OBJS += \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/RTOSDemo.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/dbsct.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/flop.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/intprg.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/main.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/printf-stdarg.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/resetprg.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/uIP_Task.o \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/vecttbl.o 

C_DEPS += \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/RTOSDemo.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/dbsct.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/flop.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/intprg.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/main.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/printf-stdarg.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/resetprg.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/uIP_Task.d \
./Demo/SuperH_SH7216_Renesas/RTOSDemo/vecttbl.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/SuperH_SH7216_Renesas/RTOSDemo/%.o: ../Demo/SuperH_SH7216_Renesas/RTOSDemo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


