################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/emac.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/http-strings.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-cgi.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-fs.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-fsdata.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/psock.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/timer.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uIP_Task.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uip.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uip_arp.c 

OBJS += \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/emac.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/http-strings.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-cgi.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-fs.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-fsdata.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/psock.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/timer.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uIP_Task.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uip.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uip_arp.o 

C_DEPS += \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/emac.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/http-strings.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-cgi.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-fs.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd-fsdata.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/httpd.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/psock.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/timer.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uIP_Task.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uip.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/uip_arp.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/%.o: ../Demo/CORTEX_LPC1768_GCC_RedSuite/src/webserver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


