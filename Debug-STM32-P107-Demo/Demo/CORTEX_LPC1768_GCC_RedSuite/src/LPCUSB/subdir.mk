################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/USB_CDC.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbcontrol.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbhw_lpc.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbinit.c \
../Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbstdreq.c 

OBJS += \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/USB_CDC.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbcontrol.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbhw_lpc.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbinit.o \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbstdreq.o 

C_DEPS += \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/USB_CDC.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbcontrol.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbhw_lpc.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbinit.d \
./Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/usbstdreq.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/%.o: ../Demo/CORTEX_LPC1768_GCC_RedSuite/src/LPCUSB/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


