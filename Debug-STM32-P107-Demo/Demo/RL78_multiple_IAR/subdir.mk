################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RL78_multiple_IAR/main.c \
../Demo/RL78_multiple_IAR/main_blinky.c \
../Demo/RL78_multiple_IAR/main_full.c 

OBJS += \
./Demo/RL78_multiple_IAR/main.o \
./Demo/RL78_multiple_IAR/main_blinky.o \
./Demo/RL78_multiple_IAR/main_full.o 

C_DEPS += \
./Demo/RL78_multiple_IAR/main.d \
./Demo/RL78_multiple_IAR/main_blinky.d \
./Demo/RL78_multiple_IAR/main_full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RL78_multiple_IAR/%.o: ../Demo/RL78_multiple_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


