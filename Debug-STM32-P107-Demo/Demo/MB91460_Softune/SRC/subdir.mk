################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/MB91460_Softune/SRC/Start91460.asm \
../Demo/MB91460_Softune/SRC/mb91467d.asm 

C_SRCS += \
../Demo/MB91460_Softune/SRC/crflash_modified.c \
../Demo/MB91460_Softune/SRC/main.c \
../Demo/MB91460_Softune/SRC/vectors.c 

OBJS += \
./Demo/MB91460_Softune/SRC/Start91460.o \
./Demo/MB91460_Softune/SRC/crflash_modified.o \
./Demo/MB91460_Softune/SRC/main.o \
./Demo/MB91460_Softune/SRC/mb91467d.o \
./Demo/MB91460_Softune/SRC/vectors.o 

ASM_DEPS += \
./Demo/MB91460_Softune/SRC/Start91460.d \
./Demo/MB91460_Softune/SRC/mb91467d.d 

C_DEPS += \
./Demo/MB91460_Softune/SRC/crflash_modified.d \
./Demo/MB91460_Softune/SRC/main.d \
./Demo/MB91460_Softune/SRC/vectors.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MB91460_Softune/SRC/%.o: ../Demo/MB91460_Softune/SRC/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/MB91460_Softune/SRC/%.o: ../Demo/MB91460_Softune/SRC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


