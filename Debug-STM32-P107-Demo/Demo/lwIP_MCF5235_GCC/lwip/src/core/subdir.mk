################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/dhcp.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/inet.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/inet6.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/mem.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/memp.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/netif.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/pbuf.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/raw.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/stats.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/sys.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp_in.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp_out.c \
../Demo/lwIP_MCF5235_GCC/lwip/src/core/udp.c 

OBJS += \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/dhcp.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/inet.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/inet6.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/mem.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/memp.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/netif.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/pbuf.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/raw.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/stats.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/sys.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp_in.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp_out.o \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/udp.o 

C_DEPS += \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/dhcp.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/inet.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/inet6.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/mem.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/memp.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/netif.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/pbuf.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/raw.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/stats.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/sys.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp_in.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/tcp_out.d \
./Demo/lwIP_MCF5235_GCC/lwip/src/core/udp.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/lwIP_MCF5235_GCC/lwip/src/core/%.o: ../Demo/lwIP_MCF5235_GCC/lwip/src/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


