################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/lwIP_MCF5235_GCC/system/init.c \
../Demo/lwIP_MCF5235_GCC/system/newlib.c \
../Demo/lwIP_MCF5235_GCC/system/serial.c 

S_UPPER_SRCS += \
../Demo/lwIP_MCF5235_GCC/system/crt0.S \
../Demo/lwIP_MCF5235_GCC/system/mcf5xxx.S \
../Demo/lwIP_MCF5235_GCC/system/vector.S 

OBJS += \
./Demo/lwIP_MCF5235_GCC/system/crt0.o \
./Demo/lwIP_MCF5235_GCC/system/init.o \
./Demo/lwIP_MCF5235_GCC/system/mcf5xxx.o \
./Demo/lwIP_MCF5235_GCC/system/newlib.o \
./Demo/lwIP_MCF5235_GCC/system/serial.o \
./Demo/lwIP_MCF5235_GCC/system/vector.o 

S_UPPER_DEPS += \
./Demo/lwIP_MCF5235_GCC/system/crt0.d \
./Demo/lwIP_MCF5235_GCC/system/mcf5xxx.d \
./Demo/lwIP_MCF5235_GCC/system/vector.d 

C_DEPS += \
./Demo/lwIP_MCF5235_GCC/system/init.d \
./Demo/lwIP_MCF5235_GCC/system/newlib.d \
./Demo/lwIP_MCF5235_GCC/system/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/lwIP_MCF5235_GCC/system/%.o: ../Demo/lwIP_MCF5235_GCC/system/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/lwIP_MCF5235_GCC/system/%.o: ../Demo/lwIP_MCF5235_GCC/system/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


