################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/IntQueueTimer.c \
../Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/main_full.c 

S_UPPER_SRCS += \
../Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/RegTest_GCC.S 

OBJS += \
./Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/IntQueueTimer.o \
./Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/RegTest_GCC.o \
./Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/main_full.o 

S_UPPER_DEPS += \
./Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/RegTest_GCC.d 

C_DEPS += \
./Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/IntQueueTimer.d \
./Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/main_full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/%.o: ../Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/%.o: ../Demo/RX200_RX231-RSK_GCC_e2studio_IAR/src/Full_Demo/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


