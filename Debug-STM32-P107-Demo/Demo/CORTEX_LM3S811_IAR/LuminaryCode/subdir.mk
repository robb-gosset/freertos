################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/adc.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/comp.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/flash.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/gpio.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/i2c.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/interrupt.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/osram96x16.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/pwm.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/qei.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/ssi.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/sysctl.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/systick.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/timer.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/uart.c \
../Demo/CORTEX_LM3S811_IAR/LuminaryCode/watchdog.c 

OBJS += \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/adc.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/comp.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/flash.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/gpio.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/i2c.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/interrupt.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/osram96x16.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/pwm.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/qei.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/ssi.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/sysctl.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/systick.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/timer.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/uart.o \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/watchdog.o 

C_DEPS += \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/adc.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/comp.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/flash.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/gpio.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/i2c.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/interrupt.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/osram96x16.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/pwm.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/qei.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/ssi.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/sysctl.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/systick.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/timer.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/uart.d \
./Demo/CORTEX_LM3S811_IAR/LuminaryCode/watchdog.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LM3S811_IAR/LuminaryCode/%.o: ../Demo/CORTEX_LM3S811_IAR/LuminaryCode/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


