################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/FreeRTOS_Tick_Setup.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/MCF52221_sysinit.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/cfm.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/exceptions.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/main.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/printf-stdarg.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/startcf.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/stdlib.c \
../Demo/ColdFire_MCF52221_CodeWarrior/sources/uart_support.c 

OBJS += \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/FreeRTOS_Tick_Setup.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/MCF52221_sysinit.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/cfm.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/exceptions.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/main.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/printf-stdarg.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/startcf.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/stdlib.o \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/uart_support.o 

C_DEPS += \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/FreeRTOS_Tick_Setup.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/MCF52221_sysinit.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/cfm.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/exceptions.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/main.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/printf-stdarg.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/startcf.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/stdlib.d \
./Demo/ColdFire_MCF52221_CodeWarrior/sources/uart_support.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ColdFire_MCF52221_CodeWarrior/sources/%.o: ../Demo/ColdFire_MCF52221_CodeWarrior/sources/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


