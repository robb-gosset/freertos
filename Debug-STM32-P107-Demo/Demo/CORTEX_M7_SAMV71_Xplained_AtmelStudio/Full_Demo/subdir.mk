################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/IntQueueTimer.c \
../Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/RegTest_GCC.c \
../Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/main_full.c 

OBJS += \
./Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/IntQueueTimer.o \
./Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/RegTest_GCC.o \
./Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/main_full.o 

C_DEPS += \
./Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/IntQueueTimer.d \
./Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/RegTest_GCC.d \
./Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/main_full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/%.o: ../Demo/CORTEX_M7_SAMV71_Xplained_AtmelStudio/Full_Demo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


