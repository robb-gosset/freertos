################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/EMAC.c \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/httpd-cgi.c \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/httpd-fsdata.c 

OBJS += \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/EMAC.o \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/httpd-cgi.o \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/httpd-fsdata.o 

C_DEPS += \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/EMAC.d \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/httpd-cgi.d \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/httpd-fsdata.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/%.o: ../Demo/CORTEX_Kinetis_K60_Tower_IAR/webserver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


