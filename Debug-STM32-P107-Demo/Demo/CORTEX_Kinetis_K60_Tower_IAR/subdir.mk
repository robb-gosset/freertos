################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/ParTest.c \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/main-full.c \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/main_blinky.c \
../Demo/CORTEX_Kinetis_K60_Tower_IAR/uIP_Task.c 

OBJS += \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/ParTest.o \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/main-full.o \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/main_blinky.o \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/uIP_Task.o 

C_DEPS += \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/ParTest.d \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/main-full.d \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/main_blinky.d \
./Demo/CORTEX_Kinetis_K60_Tower_IAR/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_Kinetis_K60_Tower_IAR/%.o: ../Demo/CORTEX_Kinetis_K60_Tower_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


