################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC24_MPLAB/lcd.c \
../Demo/PIC24_MPLAB/main.c \
../Demo/PIC24_MPLAB/timertest.c 

OBJS += \
./Demo/PIC24_MPLAB/lcd.o \
./Demo/PIC24_MPLAB/main.o \
./Demo/PIC24_MPLAB/timertest.o 

C_DEPS += \
./Demo/PIC24_MPLAB/lcd.d \
./Demo/PIC24_MPLAB/main.d \
./Demo/PIC24_MPLAB/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC24_MPLAB/%.o: ../Demo/PIC24_MPLAB/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


