################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/ParTest.c \
../Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/SAM4L_low_power_tick_management.c \
../Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main.c \
../Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main_full.c \
../Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main_low_power.c 

OBJS += \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/ParTest.o \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/SAM4L_low_power_tick_management.o \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main.o \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main_full.o \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main_low_power.o 

C_DEPS += \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/ParTest.d \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/SAM4L_low_power_tick_management.d \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main.d \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main_full.d \
./Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/main_low_power.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/%.o: ../Demo/CORTEX_M4_ATSAM4L_Atmel_Studio/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


