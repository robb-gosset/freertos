################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/IntQueueTimer.c \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/main_full.c 

S_UPPER_SRCS += \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/reg_test.S 

OBJS += \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/IntQueueTimer.o \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/main_full.o \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/reg_test.o 

S_UPPER_DEPS += \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/reg_test.d 

C_DEPS += \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/IntQueueTimer.d \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/main_full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/%.o: ../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/%.o: ../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/Full_Demo/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


