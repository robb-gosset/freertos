################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/CDCCommandConsole.c \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/FreeRTOS_tick_config.c \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/LEDs.c \
../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/main.c 

OBJS += \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/CDCCommandConsole.o \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/FreeRTOS_tick_config.o \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/LEDs.o \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/main.o 

C_DEPS += \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/CDCCommandConsole.d \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/FreeRTOS_tick_config.d \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/LEDs.d \
./Demo/CORTEX_A5_SAMA5D4x_EK_IAR/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_A5_SAMA5D4x_EK_IAR/%.o: ../Demo/CORTEX_A5_SAMA5D4x_EK_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


