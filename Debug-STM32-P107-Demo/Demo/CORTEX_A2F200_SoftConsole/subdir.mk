################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_A2F200_SoftConsole/ParTest.c \
../Demo/CORTEX_A2F200_SoftConsole/main-blinky.c \
../Demo/CORTEX_A2F200_SoftConsole/main-full.c \
../Demo/CORTEX_A2F200_SoftConsole/printf-stdarg.c \
../Demo/CORTEX_A2F200_SoftConsole/uIP_Task.c 

OBJS += \
./Demo/CORTEX_A2F200_SoftConsole/ParTest.o \
./Demo/CORTEX_A2F200_SoftConsole/main-blinky.o \
./Demo/CORTEX_A2F200_SoftConsole/main-full.o \
./Demo/CORTEX_A2F200_SoftConsole/printf-stdarg.o \
./Demo/CORTEX_A2F200_SoftConsole/uIP_Task.o 

C_DEPS += \
./Demo/CORTEX_A2F200_SoftConsole/ParTest.d \
./Demo/CORTEX_A2F200_SoftConsole/main-blinky.d \
./Demo/CORTEX_A2F200_SoftConsole/main-full.d \
./Demo/CORTEX_A2F200_SoftConsole/printf-stdarg.d \
./Demo/CORTEX_A2F200_SoftConsole/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_A2F200_SoftConsole/%.o: ../Demo/CORTEX_A2F200_SoftConsole/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


