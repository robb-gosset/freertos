################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC18_WizC/Demo2/fuses.c \
../Demo/PIC18_WizC/Demo2/interrupt.c \
../Demo/PIC18_WizC/Demo2/main.c 

OBJS += \
./Demo/PIC18_WizC/Demo2/fuses.o \
./Demo/PIC18_WizC/Demo2/interrupt.o \
./Demo/PIC18_WizC/Demo2/main.o 

C_DEPS += \
./Demo/PIC18_WizC/Demo2/fuses.d \
./Demo/PIC18_WizC/Demo2/interrupt.d \
./Demo/PIC18_WizC/Demo2/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC18_WizC/Demo2/%.o: ../Demo/PIC18_WizC/Demo2/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


