################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/RegTest.c \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/Sample-CLI-commands.c \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/UARTCommandConsole.c \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main-blinky.c \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main-full.c \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main.c \
../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/printf-stdarg.c 

OBJS += \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/RegTest.o \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/Sample-CLI-commands.o \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/UARTCommandConsole.o \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main-blinky.o \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main-full.o \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main.o \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/printf-stdarg.o 

C_DEPS += \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/RegTest.d \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/Sample-CLI-commands.d \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/UARTCommandConsole.d \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main-blinky.d \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main-full.d \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/main.d \
./Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/printf-stdarg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/%.o: ../Demo/CORTEX_M0+_Atmel_SAMD20_XPlained/RTOSDemo/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


