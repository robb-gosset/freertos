################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F107_GCC_Rowley/main.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/printf-stdarg.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/spi_flash.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/timertest.c 

OBJS += \
./Demo/CORTEX_STM32F107_GCC_Rowley/main.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/printf-stdarg.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/spi_flash.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/timertest.o 

C_DEPS += \
./Demo/CORTEX_STM32F107_GCC_Rowley/main.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/printf-stdarg.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/spi_flash.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F107_GCC_Rowley/%.o: ../Demo/CORTEX_STM32F107_GCC_Rowley/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


