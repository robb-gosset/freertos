################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/emac.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/http-strings.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd-cgi.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd-fs.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd.c \
../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/uIP_Task.c 

OBJS += \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/emac.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/http-strings.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd-cgi.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd-fs.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd.o \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/uIP_Task.o 

C_DEPS += \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/emac.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/http-strings.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd-cgi.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd-fs.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/httpd.d \
./Demo/CORTEX_STM32F107_GCC_Rowley/webserver/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_STM32F107_GCC_Rowley/webserver/%.o: ../Demo/CORTEX_STM32F107_GCC_Rowley/webserver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -DSTM32F10X_CL -DFRAME_MULTIPLE=1 -DPACK_STRUCT_END='__attribute((packed))' -DALIGN_STRUCT_END='__attribute((aligned(4)))' -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/LCD" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley/webserver" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/ethernet/FreeRTOS-uIP" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/robb-gosset/workspace/FYP/stm32f10x_stdperiph_lib/Libraries/CMSIS/CM3/CoreSupport" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/Common/drivers/ST/STM32F10xFWLib/inc" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -Wa,-adhlns="$@.lst" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


