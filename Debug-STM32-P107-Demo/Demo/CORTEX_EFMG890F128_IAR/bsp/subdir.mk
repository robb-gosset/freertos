################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_EFMG890F128_IAR/bsp/dvk.c \
../Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_boardcontrol.c \
../Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_ebi.c \
../Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_spi.c 

OBJS += \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk.o \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_boardcontrol.o \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_ebi.o \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_spi.o 

C_DEPS += \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk.d \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_boardcontrol.d \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_ebi.d \
./Demo/CORTEX_EFMG890F128_IAR/bsp/dvk_spi.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_EFMG890F128_IAR/bsp/%.o: ../Demo/CORTEX_EFMG890F128_IAR/bsp/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


