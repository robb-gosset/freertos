################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/RegTest.c \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/System_XMC4500.c \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main.c \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main_blinky.c \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main_full.c \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/system_XMC4200.c \
../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/system_XMC4400.c 

OBJS += \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/RegTest.o \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/System_XMC4500.o \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main.o \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main_blinky.o \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main_full.o \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/system_XMC4200.o \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/system_XMC4400.o 

C_DEPS += \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/RegTest.d \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/System_XMC4500.d \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main.d \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main_blinky.d \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/main_full.d \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/system_XMC4200.d \
./Demo/CORTEX_M4F_Infineon_XMC4000_Keil/system_XMC4400.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M4F_Infineon_XMC4000_Keil/%.o: ../Demo/CORTEX_M4F_Infineon_XMC4000_Keil/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


