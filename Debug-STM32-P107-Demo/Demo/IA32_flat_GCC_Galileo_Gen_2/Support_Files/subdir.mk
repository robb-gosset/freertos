################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/GPIO_I2C.c \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/HPET.c \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/freestanding_functions.c \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/galileo-support.c \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/printf-stdarg.c 

S_UPPER_SRCS += \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/startup.S 

OBJS += \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/GPIO_I2C.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/HPET.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/freestanding_functions.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/galileo-support.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/printf-stdarg.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/startup.o 

S_UPPER_DEPS += \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/startup.d 

C_DEPS += \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/GPIO_I2C.d \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/HPET.d \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/freestanding_functions.d \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/galileo-support.d \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/printf-stdarg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/%.o: ../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/%.o: ../Demo/IA32_flat_GCC_Galileo_Gen_2/Support_Files/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


