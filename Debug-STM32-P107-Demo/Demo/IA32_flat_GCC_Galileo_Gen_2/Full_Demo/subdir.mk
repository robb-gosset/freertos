################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/IntQueueTimer.c \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/main_full.c 

S_UPPER_SRCS += \
../Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/RegTest.S 

OBJS += \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/IntQueueTimer.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/RegTest.o \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/main_full.o 

S_UPPER_DEPS += \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/RegTest.d 

C_DEPS += \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/IntQueueTimer.d \
./Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/main_full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/%.o: ../Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/%.o: ../Demo/IA32_flat_GCC_Galileo_Gen_2/Full_Demo/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


