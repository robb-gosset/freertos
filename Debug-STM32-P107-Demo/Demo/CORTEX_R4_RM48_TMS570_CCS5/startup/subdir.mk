################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_core.asm \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_intvecs.asm \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_memory.asm 

C_SRCS += \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_esm.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_phantom.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_startup.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_system.c 

OBJS += \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_core.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_esm.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_intvecs.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_memory.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_phantom.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_startup.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_system.o 

ASM_DEPS += \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_core.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_intvecs.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_memory.d 

C_DEPS += \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_esm.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_phantom.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_startup.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/sys_system.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/%.o: ../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/%.o: ../Demo/CORTEX_R4_RM48_TMS570_CCS5/startup/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


