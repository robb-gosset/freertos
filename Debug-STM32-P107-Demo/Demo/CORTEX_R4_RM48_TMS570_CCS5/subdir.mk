################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/reg_test.asm 

C_SRCS += \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/ParTest.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/flop_hercules.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/main.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/main_blinky.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/main_full.c \
../Demo/CORTEX_R4_RM48_TMS570_CCS5/serial.c 

OBJS += \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/ParTest.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/flop_hercules.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/main.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/main_blinky.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/main_full.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/reg_test.o \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/serial.o 

ASM_DEPS += \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/reg_test.d 

C_DEPS += \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/ParTest.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/flop_hercules.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/main.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/main_blinky.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/main_full.d \
./Demo/CORTEX_R4_RM48_TMS570_CCS5/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_R4_RM48_TMS570_CCS5/%.o: ../Demo/CORTEX_R4_RM48_TMS570_CCS5/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_R4_RM48_TMS570_CCS5/%.o: ../Demo/CORTEX_R4_RM48_TMS570_CCS5/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


