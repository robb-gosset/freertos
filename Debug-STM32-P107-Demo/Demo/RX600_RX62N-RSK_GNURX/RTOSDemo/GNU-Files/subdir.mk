################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/start.asm 

C_SRCS += \
../Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/hwinit.c \
../Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/inthandler.c 

OBJS += \
./Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/hwinit.o \
./Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/inthandler.o \
./Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/start.o 

ASM_DEPS += \
./Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/start.d 

C_DEPS += \
./Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/hwinit.d \
./Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/inthandler.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/%.o: ../Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/%.o: ../Demo/RX600_RX62N-RSK_GNURX/RTOSDemo/GNU-Files/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


