################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/reset_program.asm 

C_SRCS += \
../Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/interrupt_handlers.c \
../Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/vector_table.c 

OBJS += \
./Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/interrupt_handlers.o \
./Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/reset_program.o \
./Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/vector_table.o 

ASM_DEPS += \
./Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/reset_program.d 

C_DEPS += \
./Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/interrupt_handlers.d \
./Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/vector_table.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/%.o: ../Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/%.o: ../Demo/RX100_RX113-RSK_GCC_e2studio_IAR/src/Renesas_Code/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


