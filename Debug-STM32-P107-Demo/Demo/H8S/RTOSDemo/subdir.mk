################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/H8S/RTOSDemo/start.asm 

C_SRCS += \
../Demo/H8S/RTOSDemo/main.c \
../Demo/H8S/RTOSDemo/vects.c 

OBJS += \
./Demo/H8S/RTOSDemo/main.o \
./Demo/H8S/RTOSDemo/start.o \
./Demo/H8S/RTOSDemo/vects.o 

ASM_DEPS += \
./Demo/H8S/RTOSDemo/start.d 

C_DEPS += \
./Demo/H8S/RTOSDemo/main.d \
./Demo/H8S/RTOSDemo/vects.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/H8S/RTOSDemo/%.o: ../Demo/H8S/RTOSDemo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/H8S/RTOSDemo/%.o: ../Demo/H8S/RTOSDemo/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


