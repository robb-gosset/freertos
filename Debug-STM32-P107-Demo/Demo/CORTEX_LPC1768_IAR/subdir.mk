################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LPC1768_IAR/ParTest.c \
../Demo/CORTEX_LPC1768_IAR/main.c \
../Demo/CORTEX_LPC1768_IAR/printf-stdarg.c 

OBJS += \
./Demo/CORTEX_LPC1768_IAR/ParTest.o \
./Demo/CORTEX_LPC1768_IAR/main.o \
./Demo/CORTEX_LPC1768_IAR/printf-stdarg.o 

C_DEPS += \
./Demo/CORTEX_LPC1768_IAR/ParTest.d \
./Demo/CORTEX_LPC1768_IAR/main.d \
./Demo/CORTEX_LPC1768_IAR/printf-stdarg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LPC1768_IAR/%.o: ../Demo/CORTEX_LPC1768_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


