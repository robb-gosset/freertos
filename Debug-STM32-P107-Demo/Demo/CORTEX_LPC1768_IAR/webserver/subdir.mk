################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LPC1768_IAR/webserver/emac.c \
../Demo/CORTEX_LPC1768_IAR/webserver/http-strings.c \
../Demo/CORTEX_LPC1768_IAR/webserver/httpd-cgi.c \
../Demo/CORTEX_LPC1768_IAR/webserver/httpd-fs.c \
../Demo/CORTEX_LPC1768_IAR/webserver/httpd-fsdata.c \
../Demo/CORTEX_LPC1768_IAR/webserver/httpd.c \
../Demo/CORTEX_LPC1768_IAR/webserver/uIP_Task.c 

OBJS += \
./Demo/CORTEX_LPC1768_IAR/webserver/emac.o \
./Demo/CORTEX_LPC1768_IAR/webserver/http-strings.o \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd-cgi.o \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd-fs.o \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd-fsdata.o \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd.o \
./Demo/CORTEX_LPC1768_IAR/webserver/uIP_Task.o 

C_DEPS += \
./Demo/CORTEX_LPC1768_IAR/webserver/emac.d \
./Demo/CORTEX_LPC1768_IAR/webserver/http-strings.d \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd-cgi.d \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd-fs.d \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd-fsdata.d \
./Demo/CORTEX_LPC1768_IAR/webserver/httpd.d \
./Demo/CORTEX_LPC1768_IAR/webserver/uIP_Task.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LPC1768_IAR/webserver/%.o: ../Demo/CORTEX_LPC1768_IAR/webserver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


