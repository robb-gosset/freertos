################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/MSP430X_MSP430F5438_IAR/ParTest.c \
../Demo/MSP430X_MSP430F5438_IAR/RunTimeStatsConfig.c \
../Demo/MSP430X_MSP430F5438_IAR/low_level_init.c \
../Demo/MSP430X_MSP430F5438_IAR/main.c \
../Demo/MSP430X_MSP430F5438_IAR/serial.c 

OBJS += \
./Demo/MSP430X_MSP430F5438_IAR/ParTest.o \
./Demo/MSP430X_MSP430F5438_IAR/RunTimeStatsConfig.o \
./Demo/MSP430X_MSP430F5438_IAR/low_level_init.o \
./Demo/MSP430X_MSP430F5438_IAR/main.o \
./Demo/MSP430X_MSP430F5438_IAR/serial.o 

C_DEPS += \
./Demo/MSP430X_MSP430F5438_IAR/ParTest.d \
./Demo/MSP430X_MSP430F5438_IAR/RunTimeStatsConfig.d \
./Demo/MSP430X_MSP430F5438_IAR/low_level_init.d \
./Demo/MSP430X_MSP430F5438_IAR/main.d \
./Demo/MSP430X_MSP430F5438_IAR/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MSP430X_MSP430F5438_IAR/%.o: ../Demo/MSP430X_MSP430F5438_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


