################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/FreeRTOS_tick_config.c \
../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/ParTest.c \
../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/main.c \
../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/platform.c \
../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/printf-stdarg.c 

S_UPPER_SRCS += \
../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/FreeRTOS_asm_vectors.S 

OBJS += \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/FreeRTOS_asm_vectors.o \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/FreeRTOS_tick_config.o \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/ParTest.o \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/main.o \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/platform.o \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/printf-stdarg.o 

S_UPPER_DEPS += \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/FreeRTOS_asm_vectors.d 

C_DEPS += \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/FreeRTOS_tick_config.d \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/ParTest.d \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/main.d \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/platform.d \
./Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/printf-stdarg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/%.o: ../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/%.o: ../Demo/CORTEX_A9_Zynq_ZC702/RTOSDemo/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


