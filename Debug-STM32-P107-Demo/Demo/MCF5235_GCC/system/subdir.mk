################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/MCF5235_GCC/system/init.c \
../Demo/MCF5235_GCC/system/newlib.c \
../Demo/MCF5235_GCC/system/serial.c 

S_UPPER_SRCS += \
../Demo/MCF5235_GCC/system/crt0.S \
../Demo/MCF5235_GCC/system/mcf5xxx.S \
../Demo/MCF5235_GCC/system/vector.S 

OBJS += \
./Demo/MCF5235_GCC/system/crt0.o \
./Demo/MCF5235_GCC/system/init.o \
./Demo/MCF5235_GCC/system/mcf5xxx.o \
./Demo/MCF5235_GCC/system/newlib.o \
./Demo/MCF5235_GCC/system/serial.o \
./Demo/MCF5235_GCC/system/vector.o 

S_UPPER_DEPS += \
./Demo/MCF5235_GCC/system/crt0.d \
./Demo/MCF5235_GCC/system/mcf5xxx.d \
./Demo/MCF5235_GCC/system/vector.d 

C_DEPS += \
./Demo/MCF5235_GCC/system/init.d \
./Demo/MCF5235_GCC/system/newlib.d \
./Demo/MCF5235_GCC/system/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MCF5235_GCC/system/%.o: ../Demo/MCF5235_GCC/system/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/MCF5235_GCC/system/%.o: ../Demo/MCF5235_GCC/system/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


