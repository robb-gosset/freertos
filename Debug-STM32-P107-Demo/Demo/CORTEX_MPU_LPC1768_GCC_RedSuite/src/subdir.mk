################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/cr_startup_lpc17.c \
../Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/main.c \
../Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/printf-stdarg.c \
../Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/syscalls.c 

OBJS += \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/cr_startup_lpc17.o \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/main.o \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/printf-stdarg.o \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/syscalls.o 

C_DEPS += \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/cr_startup_lpc17.d \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/main.d \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/printf-stdarg.d \
./Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/%.o: ../Demo/CORTEX_MPU_LPC1768_GCC_RedSuite/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


