################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/icmp.c \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip.c \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip_addr.c \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip_frag.c 

OBJS += \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/icmp.o \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip.o \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip_addr.o \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip_frag.o 

C_DEPS += \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/icmp.d \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip.d \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip_addr.d \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/ip_frag.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/%.o: ../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/core/ipv4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


