################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/etharp.c \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/ethernetif.c \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/loopif.c \
../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/slipif.c 

OBJS += \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/etharp.o \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/ethernetif.o \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/loopif.o \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/slipif.o 

C_DEPS += \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/etharp.d \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/ethernetif.d \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/loopif.d \
./Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/slipif.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/%.o: ../Demo/lwIP_Demo_Rowley_ARM7/lwip-1.1.0/src/netif/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


