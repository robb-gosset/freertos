################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/lwIP_Demo_Rowley_ARM7/USB/USB-CDC.c \
../Demo/lwIP_Demo_Rowley_ARM7/USB/USBIsr.c 

OBJS += \
./Demo/lwIP_Demo_Rowley_ARM7/USB/USB-CDC.o \
./Demo/lwIP_Demo_Rowley_ARM7/USB/USBIsr.o 

C_DEPS += \
./Demo/lwIP_Demo_Rowley_ARM7/USB/USB-CDC.d \
./Demo/lwIP_Demo_Rowley_ARM7/USB/USBIsr.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/lwIP_Demo_Rowley_ARM7/USB/%.o: ../Demo/lwIP_Demo_Rowley_ARM7/USB/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


