################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/IntQueueTimer.c \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/formike128x128x16.c \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/main.c \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/osram128x64x4.c \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/rit128x96x4.c \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/startup_ewarm.c \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/timertest.c 

S_UPPER_SRCS += \
../Demo/CORTEX_LM3Sxxxx_IAR_Keil/startup_rvmdk.S 

OBJS += \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/IntQueueTimer.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/formike128x128x16.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/main.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/osram128x64x4.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/rit128x96x4.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/startup_ewarm.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/startup_rvmdk.o \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/timertest.o 

S_UPPER_DEPS += \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/startup_rvmdk.d 

C_DEPS += \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/IntQueueTimer.d \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/formike128x128x16.d \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/main.d \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/osram128x64x4.d \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/rit128x96x4.d \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/startup_ewarm.d \
./Demo/CORTEX_LM3Sxxxx_IAR_Keil/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LM3Sxxxx_IAR_Keil/%.o: ../Demo/CORTEX_LM3Sxxxx_IAR_Keil/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/CORTEX_LM3Sxxxx_IAR_Keil/%.o: ../Demo/CORTEX_LM3Sxxxx_IAR_Keil/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


