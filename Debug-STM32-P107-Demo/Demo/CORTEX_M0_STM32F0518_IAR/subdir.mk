################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M0_STM32F0518_IAR/ParTest.c \
../Demo/CORTEX_M0_STM32F0518_IAR/main-blinky.c \
../Demo/CORTEX_M0_STM32F0518_IAR/main-full.c \
../Demo/CORTEX_M0_STM32F0518_IAR/main.c 

OBJS += \
./Demo/CORTEX_M0_STM32F0518_IAR/ParTest.o \
./Demo/CORTEX_M0_STM32F0518_IAR/main-blinky.o \
./Demo/CORTEX_M0_STM32F0518_IAR/main-full.o \
./Demo/CORTEX_M0_STM32F0518_IAR/main.o 

C_DEPS += \
./Demo/CORTEX_M0_STM32F0518_IAR/ParTest.d \
./Demo/CORTEX_M0_STM32F0518_IAR/main-blinky.d \
./Demo/CORTEX_M0_STM32F0518_IAR/main-full.d \
./Demo/CORTEX_M0_STM32F0518_IAR/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M0_STM32F0518_IAR/%.o: ../Demo/CORTEX_M0_STM32F0518_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


