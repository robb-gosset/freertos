################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/InterruptNestTest.c \
../Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/ParTest.c \
../Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/main.c \
../Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/serial.c 

OBJS += \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/InterruptNestTest.o \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/ParTest.o \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/main.o \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/serial.o 

C_DEPS += \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/InterruptNestTest.d \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/ParTest.d \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/main.d \
./Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/serial.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/%.o: ../Demo/TriCore_TC1782_TriBoard_GCC/RTOSDemo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


