################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX600_RX64M_RSK_GCC_e2studio/src/IntQueueTimer.c \
../Demo/RX600_RX64M_RSK_GCC_e2studio/src/ParTest.c \
../Demo/RX600_RX64M_RSK_GCC_e2studio/src/main.c \
../Demo/RX600_RX64M_RSK_GCC_e2studio/src/main_blinky.c \
../Demo/RX600_RX64M_RSK_GCC_e2studio/src/main_full.c 

S_UPPER_SRCS += \
../Demo/RX600_RX64M_RSK_GCC_e2studio/src/RegTest.S 

OBJS += \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/IntQueueTimer.o \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/ParTest.o \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/RegTest.o \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/main.o \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/main_blinky.o \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/main_full.o 

S_UPPER_DEPS += \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/RegTest.d 

C_DEPS += \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/IntQueueTimer.d \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/ParTest.d \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/main.d \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/main_blinky.d \
./Demo/RX600_RX64M_RSK_GCC_e2studio/src/main_full.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX600_RX64M_RSK_GCC_e2studio/src/%.o: ../Demo/RX600_RX64M_RSK_GCC_e2studio/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/RX600_RX64M_RSK_GCC_e2studio/src/%.o: ../Demo/RX600_RX64M_RSK_GCC_e2studio/src/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


