################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/PIC32MZ_MPLAB/ConfigPerformance.c \
../Demo/PIC32MZ_MPLAB/ISRTriggeredTask.c \
../Demo/PIC32MZ_MPLAB/IntQueueTimer.c \
../Demo/PIC32MZ_MPLAB/flop_mz.c \
../Demo/PIC32MZ_MPLAB/main.c \
../Demo/PIC32MZ_MPLAB/main_blinky.c \
../Demo/PIC32MZ_MPLAB/main_full.c \
../Demo/PIC32MZ_MPLAB/timertest.c 

S_UPPER_SRCS += \
../Demo/PIC32MZ_MPLAB/ISRTriggeredTask_isr.S \
../Demo/PIC32MZ_MPLAB/IntQueueTimer_isr.S \
../Demo/PIC32MZ_MPLAB/RegisterTestTasks.S \
../Demo/PIC32MZ_MPLAB/timertest_isr.S 

OBJS += \
./Demo/PIC32MZ_MPLAB/ConfigPerformance.o \
./Demo/PIC32MZ_MPLAB/ISRTriggeredTask.o \
./Demo/PIC32MZ_MPLAB/ISRTriggeredTask_isr.o \
./Demo/PIC32MZ_MPLAB/IntQueueTimer.o \
./Demo/PIC32MZ_MPLAB/IntQueueTimer_isr.o \
./Demo/PIC32MZ_MPLAB/RegisterTestTasks.o \
./Demo/PIC32MZ_MPLAB/flop_mz.o \
./Demo/PIC32MZ_MPLAB/main.o \
./Demo/PIC32MZ_MPLAB/main_blinky.o \
./Demo/PIC32MZ_MPLAB/main_full.o \
./Demo/PIC32MZ_MPLAB/timertest.o \
./Demo/PIC32MZ_MPLAB/timertest_isr.o 

S_UPPER_DEPS += \
./Demo/PIC32MZ_MPLAB/ISRTriggeredTask_isr.d \
./Demo/PIC32MZ_MPLAB/IntQueueTimer_isr.d \
./Demo/PIC32MZ_MPLAB/RegisterTestTasks.d \
./Demo/PIC32MZ_MPLAB/timertest_isr.d 

C_DEPS += \
./Demo/PIC32MZ_MPLAB/ConfigPerformance.d \
./Demo/PIC32MZ_MPLAB/ISRTriggeredTask.d \
./Demo/PIC32MZ_MPLAB/IntQueueTimer.d \
./Demo/PIC32MZ_MPLAB/flop_mz.d \
./Demo/PIC32MZ_MPLAB/main.d \
./Demo/PIC32MZ_MPLAB/main_blinky.d \
./Demo/PIC32MZ_MPLAB/main_full.d \
./Demo/PIC32MZ_MPLAB/timertest.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/PIC32MZ_MPLAB/%.o: ../Demo/PIC32MZ_MPLAB/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/PIC32MZ_MPLAB/%.o: ../Demo/PIC32MZ_MPLAB/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


