################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/RX100-RSK_IAR/ParTest.c \
../Demo/RX100-RSK_IAR/main.c \
../Demo/RX100-RSK_IAR/main_full.c \
../Demo/RX100-RSK_IAR/main_low_power.c 

OBJS += \
./Demo/RX100-RSK_IAR/ParTest.o \
./Demo/RX100-RSK_IAR/main.o \
./Demo/RX100-RSK_IAR/main_full.o \
./Demo/RX100-RSK_IAR/main_low_power.o 

C_DEPS += \
./Demo/RX100-RSK_IAR/ParTest.d \
./Demo/RX100-RSK_IAR/main.d \
./Demo/RX100-RSK_IAR/main_full.d \
./Demo/RX100-RSK_IAR/main_low_power.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX100-RSK_IAR/%.o: ../Demo/RX100-RSK_IAR/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


