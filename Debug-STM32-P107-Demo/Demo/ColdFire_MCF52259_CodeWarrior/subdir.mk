################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ColdFire_MCF52259_CodeWarrior/FreeRTOS_Tick_Setup.c \
../Demo/ColdFire_MCF52259_CodeWarrior/HTTPDemo.c \
../Demo/ColdFire_MCF52259_CodeWarrior/main.c \
../Demo/ColdFire_MCF52259_CodeWarrior/printf-stdarg.c \
../Demo/ColdFire_MCF52259_CodeWarrior/stdlib.c 

OBJS += \
./Demo/ColdFire_MCF52259_CodeWarrior/FreeRTOS_Tick_Setup.o \
./Demo/ColdFire_MCF52259_CodeWarrior/HTTPDemo.o \
./Demo/ColdFire_MCF52259_CodeWarrior/main.o \
./Demo/ColdFire_MCF52259_CodeWarrior/printf-stdarg.o \
./Demo/ColdFire_MCF52259_CodeWarrior/stdlib.o 

C_DEPS += \
./Demo/ColdFire_MCF52259_CodeWarrior/FreeRTOS_Tick_Setup.d \
./Demo/ColdFire_MCF52259_CodeWarrior/HTTPDemo.d \
./Demo/ColdFire_MCF52259_CodeWarrior/main.d \
./Demo/ColdFire_MCF52259_CodeWarrior/printf-stdarg.d \
./Demo/ColdFire_MCF52259_CodeWarrior/stdlib.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ColdFire_MCF52259_CodeWarrior/%.o: ../Demo/ColdFire_MCF52259_CodeWarrior/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


