################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5225x.c \
../Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5225x_sysinit.c \
../Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5xxx.c 

OBJS += \
./Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5225x.o \
./Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5225x_sysinit.o \
./Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5xxx.o 

C_DEPS += \
./Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5225x.d \
./Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5225x_sysinit.d \
./Demo/ColdFire_MCF52259_CodeWarrior/cpu/mcf5xxx.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ColdFire_MCF52259_CodeWarrior/cpu/%.o: ../Demo/ColdFire_MCF52259_CodeWarrior/cpu/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


