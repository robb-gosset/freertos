################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_adc.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_can.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_cfg.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_dma.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_eic.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_extit.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_gpio.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_i2c.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_it.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_lib.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_mrcc.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_pwm.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_rtc.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_smi.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_ssp.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_tb.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_tim.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_uart.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/75x_wdg.c \
../Demo/ARM7_STR75x_IAR/STLibrary/src/lcd.c 

OBJS += \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_adc.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_can.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_cfg.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_dma.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_eic.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_extit.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_gpio.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_i2c.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_it.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_lib.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_mrcc.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_pwm.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_rtc.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_smi.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_ssp.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_tb.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_tim.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_uart.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_wdg.o \
./Demo/ARM7_STR75x_IAR/STLibrary/src/lcd.o 

C_DEPS += \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_adc.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_can.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_cfg.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_dma.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_eic.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_extit.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_gpio.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_i2c.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_it.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_lib.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_mrcc.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_pwm.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_rtc.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_smi.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_ssp.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_tb.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_tim.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_uart.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/75x_wdg.d \
./Demo/ARM7_STR75x_IAR/STLibrary/src/lcd.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ARM7_STR75x_IAR/STLibrary/src/%.o: ../Demo/ARM7_STR75x_IAR/STLibrary/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


