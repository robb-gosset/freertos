################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/IntQueueTimer.c \
../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/RegTest.c \
../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/cr_startup_lpc11.c \
../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main-blinky.c \
../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main-full.c \
../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main.c 

OBJS += \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/IntQueueTimer.o \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/RegTest.o \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/cr_startup_lpc11.o \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main-blinky.o \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main-full.o \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main.o 

C_DEPS += \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/IntQueueTimer.d \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/RegTest.d \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/cr_startup_lpc11.d \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main-blinky.d \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main-full.d \
./Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/main.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/%.o: ../Demo/CORTEX_M0_LPC1114_LPCXpresso/RTOSDemo/Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


