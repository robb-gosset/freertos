################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/reset_program.asm 

C_SRCS += \
../Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/interrupt_handlers.c \
../Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/vector_table.c 

OBJS += \
./Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/interrupt_handlers.o \
./Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/reset_program.o \
./Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/vector_table.o 

ASM_DEPS += \
./Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/reset_program.d 

C_DEPS += \
./Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/interrupt_handlers.d \
./Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/vector_table.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/%.o: ../Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/%.o: ../Demo/RX700_RX71M_RSK_GCC_e2studio_IAR/src/Renesas_Source/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


