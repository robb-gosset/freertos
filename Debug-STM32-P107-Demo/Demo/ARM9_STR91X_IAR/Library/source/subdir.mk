################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/ARM9_STR91X_IAR/Library/source/91x_can.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_enet.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_fmi.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_gpio.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_it.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_lib.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_scu.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_tim.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_uart.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_vic.c \
../Demo/ARM9_STR91X_IAR/Library/source/91x_wdg.c 

OBJS += \
./Demo/ARM9_STR91X_IAR/Library/source/91x_can.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_enet.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_fmi.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_gpio.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_it.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_lib.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_scu.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_tim.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_uart.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_vic.o \
./Demo/ARM9_STR91X_IAR/Library/source/91x_wdg.o 

C_DEPS += \
./Demo/ARM9_STR91X_IAR/Library/source/91x_can.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_enet.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_fmi.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_gpio.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_it.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_lib.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_scu.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_tim.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_uart.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_vic.d \
./Demo/ARM9_STR91X_IAR/Library/source/91x_wdg.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/ARM9_STR91X_IAR/Library/source/%.o: ../Demo/ARM9_STR91X_IAR/Library/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


