################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Demo/MB96350_Softune_Dice_Kit/mb96356rs.asm 

C_SRCS += \
../Demo/MB96350_Softune_Dice_Kit/DiceTask.c \
../Demo/MB96350_Softune_Dice_Kit/SegmentToggleTasks.c \
../Demo/MB96350_Softune_Dice_Kit/main.c \
../Demo/MB96350_Softune_Dice_Kit/vectors.c 

OBJS += \
./Demo/MB96350_Softune_Dice_Kit/DiceTask.o \
./Demo/MB96350_Softune_Dice_Kit/SegmentToggleTasks.o \
./Demo/MB96350_Softune_Dice_Kit/main.o \
./Demo/MB96350_Softune_Dice_Kit/mb96356rs.o \
./Demo/MB96350_Softune_Dice_Kit/vectors.o 

ASM_DEPS += \
./Demo/MB96350_Softune_Dice_Kit/mb96356rs.d 

C_DEPS += \
./Demo/MB96350_Softune_Dice_Kit/DiceTask.d \
./Demo/MB96350_Softune_Dice_Kit/SegmentToggleTasks.d \
./Demo/MB96350_Softune_Dice_Kit/main.d \
./Demo/MB96350_Softune_Dice_Kit/vectors.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/MB96350_Softune_Dice_Kit/%.o: ../Demo/MB96350_Softune_Dice_Kit/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Demo/MB96350_Softune_Dice_Kit/%.o: ../Demo/MB96350_Softune_Dice_Kit/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -x assembler-with-cpp -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


