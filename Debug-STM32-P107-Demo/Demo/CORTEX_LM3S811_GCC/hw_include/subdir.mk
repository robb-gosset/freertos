################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Demo/CORTEX_LM3S811_GCC/hw_include/adc.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/comp.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/flash.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/gpio.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/i2c.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/interrupt.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/osram96x16.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/pwm.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/qei.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/ssi.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/sysctl.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/systick.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/timer.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/uart.c \
../Demo/CORTEX_LM3S811_GCC/hw_include/watchdog.c 

OBJS += \
./Demo/CORTEX_LM3S811_GCC/hw_include/adc.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/comp.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/flash.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/gpio.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/i2c.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/interrupt.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/osram96x16.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/pwm.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/qei.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/ssi.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/sysctl.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/systick.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/timer.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/uart.o \
./Demo/CORTEX_LM3S811_GCC/hw_include/watchdog.o 

C_DEPS += \
./Demo/CORTEX_LM3S811_GCC/hw_include/adc.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/comp.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/flash.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/gpio.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/i2c.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/interrupt.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/osram96x16.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/pwm.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/qei.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/ssi.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/sysctl.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/systick.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/timer.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/uart.d \
./Demo/CORTEX_LM3S811_GCC/hw_include/watchdog.d 


# Each subdirectory must supply rules for building sources it contributes
Demo/CORTEX_LM3S811_GCC/hw_include/%.o: ../Demo/CORTEX_LM3S811_GCC/hw_include/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/include" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/home/robb-gosset/workspace/FYP/FreeRTOS/Demo/CORTEX_STM32F107_GCC_Rowley" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


